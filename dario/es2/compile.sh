#!/bin/zsh

rm -rf build
rm -rf install
mkdir build
mkdir install
mkdir data

CURRENT_PATH=$PWD

cd build

cmake -DCMAKE_INSTALL_PREFIX="$CURRENT_PATH/install" ..
make -j4
make install

# g++ -o emconst ../emconst.cpp
# g++ -o emconst-float -DSINGLEPRECISION ../emconst.cpp
# g++ -o emconst-quad -DQUADRUPLEPRECISION ../emconst.cpp
# g++ -o emconst-test -DTESTDOUBLEPRECISION ../emconst.cpp
cd ..

# rsync -au --stats $CURRENT_PATH/ compute:/root/stat/es2