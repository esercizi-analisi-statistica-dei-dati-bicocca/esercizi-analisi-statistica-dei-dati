\documentclass[relazione.tex]{subfiles}


\begin{document}

\chapter{Primo Esercizio}

\section{Prima Richiesta}

La prima richiesta di questo esercizio è generare numeri casuali distribuiti come una Breit-Wigner. A questo scopo, si costruisce un generatore di numeri pseudo-casuali, \emph{pseudo-random number generator} o abbreviato \emph{prng} in inglese, uniformi tra 0 e 1. In seguito si usano i metodi della funzione inversa e hit-or-miss di von Neumann
per distribuire i numeri casuali secondo la distribuzione desiderata.

\subsection{Generatore numeri pseudo-casuali uniforme}

Si comincia costruendo un generatore lineare congruente, abbreviato con l'acronimo inglese \emph{LCG}. Questo è un generatore ricorsivo, definito dalla seguente relazione di ricorrenza:

\[X_n = (aX_{n-1}+b) \ mod \ m\]

dove i parametri, tutti numeri interi, del generatore sono:
\begin{itemize}
	\item $X$ è la sequenza di numeri pseudo-casuali
	\item $m, \ m>0$ è il \emph{modulo}
	\item $a, \ 0<a<m$ è il \emph{moltiplicatore}
	\item $c, \ 0\leq c<m$ è l'\emph{incremento}
	\item $X_0, \ 0 \leq X_0 < m$ è il \emph{seed}
\end{itemize}

Si scelgono i parametri dell'implementazione della funzione \emph{rand()} dell'architettura MMIX di Donald Knuth, cioè:

\begin{itemize}
	\item $m=2^{64}$
	\item $a=6364136223846793005$
	\item $c=1442695040888963407$
\end{itemize}

Dal momento che il modulo desiderato è di 64 bit, si sceglie di usare delle variabili \emph{long long int}, che occupano esattamente 64 bit nell'ambiente usato. Pertanto non è necessaria l'operazione del calcolo del modulo, essendo intrinseca nella rappresentazione della variabile. L'operazione eseguita a ogni step si riduce a 
\begin{minted}{c}
randtype prngLcg::randInt(){
 x = ( a * x + c );
 return x;
}
\end{minted}

\subsection{Breit-Wigner}

Avendo a disposizione un generatore uniforme, si prosegue con l'estrazione di numeri casuali distribuiti secondo una Breit-Wigner, la cui pdf ha espressione
\[f(x; x_0,\gamma) = { 1 \over \pi \gamma } \left[ { \gamma^2 \over (x - x_0)^2 + \gamma^2  } \right]\]
conseguentemente la cdf è
\[F(x; x_0,\gamma)=\frac{1}{\pi} \arctan\left(\frac{x-x_0}{\gamma}\right)+\frac{1}{2}\]
la quale ammette inversa, ed è la funzione quantile
\[Q(p; x_0,\gamma) = x_0 + \gamma\,\tan\left[\pi\left(p-\frac{1}{2}\right)\right].\]

\subsubsection{Metodo dell'inversa}

Generando numeri casuali $p \in [0,1]$ e calcolando $x = F^{-1}(p) = Q(p) $, si ottiene $x$ distribuito come $f(x)$. Si osservi che la trasformazione permette di generare numeri casuali distribuiti secondo la pdf $f$ e con supporto tutto l'asse reale. 

Viene eseguito un fit con ROOT per controllare che la distribuzione sia effettivamente una Breit-Wigner e non siano stati commessi errori

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{../es1/analysis/data-breit_new}
	\caption{}
	\label{fig:data-breit}
\end{figure}

\subsubsection{Metodo Hit-or-Miss di von Neumann}

Dopo aver scelto un range $[xmin, xmax]$ di interesse del supporto della pdf, si estrae un primo numero $xrand$ distribuito uniformemente dentro a quel range. Si genera quindi un secondo numero uniforme $yrand$ in un range che copre almeno $[0, max_{[xmin, xmax]}f(x)$. Si accetta il numero casuale $xrand$ se $yrand<f(xrand)$

Questo metodo risulta più lento rispetto al precedente e non genera i numeri casuali su tutto il supporto della pdf, però non richiede che la cdf sia invertibile. La scelta del metodo è dipendente quindi dalle caratteristiche del problema.

Questo metodo permette, ad esempio, di generare numeri distribuiti secondo una distribuzione Breit-Wigner Relativistica.

\[f(E;M,\Gamma) = \frac{k}{\left(E^2-M^2\right)^2+M^2\Gamma^2}\]

con

\[k = \frac{2 \sqrt{2} M \Gamma  \gamma }{\pi \sqrt{M^2+\gamma}}\]
e

\[\gamma=\sqrt{M^2\left(M^2+\Gamma^2\right)}\]

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{../es1/analysis/data-breitreltac_new}
	\caption{}
	\label{fig:data-breittacnew}
\end{figure}

\subsection{Listing}

Classe generatore lineare congruente: prngLcg.h
\inputminted[]{c}{../es1/prng/prngLcg.h}
e l'implementazione prngLcg.cpp
\inputminted[]{c}{../es1/prng/prngLcg.cpp}

Classe generatore come una Breit-Wigner: prngGen.h
\inputminted[]{c}{../es1/prng/prngGen.h}
implementazione prngGen.cpp
\inputminted[]{c}{../es1/prng/prngGen.cpp}
Generatore Breit-Wigner:
\inputminted[]{c}{../es1/prng/ut_prngGenInv.cpp}
Generatore Breit-Wigner relativistica:
\inputminted[]{c}{../es1/prng/ut_prngGenRelTac.cpp}

macro ROOT per il controllo delle distribuzioni
\inputminted[]{c}{../es1/analysis/analysisBreit.C}
\inputminted[]{c}{../es1/analysis/analysisBreitRel.C}

\section{Test PRNG uniforme}

Viene usata la suite \emph{dieharder}\cite{website:dieharder} per testare la bontà del generatore lineare congruente. 

Per interfacciare il generatore con \emph{dieharder} si costruisce un programma che ha come output uno stream di bit, che sono passati a dieharder attraverso il \emph{piping}.
L'output è generato con:
\begin{minted}{c}
randomnumber = generator.randInt(); 
std::cout.write(reinterpret_cast<char*>(&randomnumber),
sizeof randomnumber) ;
\end{minted}

I risultati sono i seguenti:

\inputminted[]{c}{../es1/analysis/dieharder-lcg}

Si osserva che questo generatore fallisce alcuni test e in altri e considerato debole. Se cercano quindi dei generatori migliori.

\subsection{xorshift and xoroshiro}

Si implementano dei generatori derivati da xorshift di George Marsaglia. In particolare si analizzano \emph{xorshift128plus} e \emph{xorshift1024star}, proposti da Sebastiano Vigna in \cite{DBLP:journals/corr/Vigna14} e \emph{xoroshiro128plus}, proposto sempre da Vigna in \cite{1805.01407}. L'autore propone delle implementazioni in C dei generatori disponibili ai seguenti indirizzi web:

\begin{itemize}
\item \url{http://vigna.di.unimi.it/xorshift/xorshift128plus.c}
\item \url{http://vigna.di.unimi.it/xorshift/xorshift1024star.c}
\item \url{http://vigna.di.unimi.it/xorshift/xoroshiro128plus.c}
\end{itemize}

Questi generatori hanno le seguenti caratteristiche\footnote{Una breve descrizione di questi algoritmi è disponibile a \url{https://en.wikipedia.org/wiki/Xorshift} e \url{https://en.wikipedia.org/wiki/Xoroshiro128+}}:

\begin{itemize}
	\item Se il loro nome finisce con \emph{star}, allora prima di restituire il risultato tipico di xorshift, è eseguita un'ulteriore moltiplicazione come trasformazione non lineare.
	\item Se il loro nome finisce con \emph{plus}, allora prima di restituire il risultato tipico di xorshift, è eseguita una somma, che è più veloce della moltiplicazione.
	\item lo stato interno ha le dimensioni in bit uguali al numero $nbit$ che è all'interno del nome del generatore stesso. L'output del generatore ha perido $2^{nbit}-1$
	\item xoroshiro prende il nome dalle operazioni eseguite al suo interno: XOR, rotate, shift, rotate
\end{itemize}

Questi vengono reimplementati qui in C++.

Si generano numeri casuali con \emph{xorshift128plus} tra -10 e 10 con:
\begin{minted}{C}
prngXorshift128 generator;
for (int i=0; i<N; i++){
    std::cout << generator.rand(-10, 10) << std::endl;
}
\end{minted}

Il cui generatore di base è
\begin{minted}{C}
randtype prngXorshift128::xorshift128plus(){
  randtype x = s[0] ;
  randtype y = s[1] ;
  s[0] = y ;
  x ^= x << a ; // XOR and SHIFT
  s[1] = x ^ y ^ ( x >> b) ^ ( y << 26 ) ; // XOR and SHIFT
  return s[1] + y ; // PLUS operation
}
\end{minted}



\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../es1/analysis/xorshift128_new}
	\caption{Generazione di numeri psudo-casuali distribuiti uniformemente nell'intervallo [-10,10]}
	\label{fig:xorshift128new}
\end{figure}

Si testano questi generatori sempre con \emph{dieharder}, ottenendo i seguenti risultati:

\inputminted[]{c}{../es1/analysis/dieharder-xorshift}
\inputminted[]{c}{../es1/analysis/dieharder-xorshift1024}
\inputminted[]{c}{../es1/analysis/dieharder-xoroshiro128}

Si osserva che questi generatori non falliscono nessun test di \emph{dieharder}, pur essendo ognuno debole in qualche test.

\subsection{Listings}

Interfaccia LCG-dieharder:
\inputminted[]{c}{../es1/prng/ut_prngLcgDieharder.cpp}

Classe xorshift128star: prngXorshift128.h
\inputminted[]{c}{../es1/prng/prngXorshift128.h}
implementazione in prngXorshift128.cpp
\inputminted[]{c}{../es1/prng/prngXorshift128.cpp}

Classe xorshift128star: prngXorshift1024.h
\inputminted[]{c}{../es1/prng/prngXorshift1024.h}
implementazione in prngXorshift1024.cpp
\inputminted[]{c}{../es1/prng/prngXorshift1024.cpp}

Classe xorshift128star: prngXoroshiro128.h
\inputminted[]{c}{../es1/prng/prngXoroshiro128.h}
implementazione in prngXoroshiro128.cpp
\inputminted[]{c}{../es1/prng/prngXoroshiro128.cpp}

Output per dieharder (non sono riportate le unit tet per le altre classi, essendo molto simili)
\inputminted[]{c}{../es1/prng/ut_prngXoroshiro128Dieharder.cpp}

\end{document}
