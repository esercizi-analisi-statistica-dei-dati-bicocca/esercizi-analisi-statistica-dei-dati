
    factory->BookMethod( dataloader, TMVA::Types::kFisher, "Fisher", 
        "H:!V:Fisher:VarTransform=None:CreateMVAPdfs:
        PDFInterpolMVAPdf=Spline2:NbinsMVAPdf=50:NsmoothMVAPdf=10" );
    factory->BookMethod( dataloader, TMVA::Types::kMLP, "MLP", 
        "H:!V:VarTransform=N:NCycles=600:HiddenLayers=N+5:
        TestRate=5:!UseRegulator" );
    factory->BookMethod( dataloader, TMVA::Types::kLikelihood, "Likelihood",
        "H:!V:TransformOutput:PDFInterpol=Spline2:NSmoothSig[0]=20:
        NSmoothBkg[0]=20:NSmoothBkg[1]=10:NSmooth=1:NAvEvtPerBin=50" );
    factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT", 
        "!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:
        AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:
        SeparationType=GiniIndex:nCuts=20" );