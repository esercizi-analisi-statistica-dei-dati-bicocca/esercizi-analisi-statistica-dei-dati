#include "prngGen.h"
#include "prngLcg.h"
#include <cmath>

#include <iostream>

prngGen::prngGen(){
    pi = atan(1)*4;
}
prngGen::~prngGen(){}

double prngGen::BreitWigner(double x, double peak , double hwhm){
    return hwhm / (pi * ((x - peak)*(x - peak) + hwhm*hwhm )) ;
}

double prngGen::BreitWignerINV (double peak , double hwhm ){
    return peak + hwhm * tan( pi * ( uniform.rand() - 0.5 ) ) ;
}

double prngGen::BreitWignerTAC (double peak , double hwhm, 
    double xmin, double xmax, double ymin, double ymax ){
    double xrand = uniform.rand(xmin, xmax) ;
    double yrand = uniform.rand(ymin, ymax) ;
    double ybreit = BreitWigner( xrand, peak , hwhm) ;
    // accept xrand only if yrand is <= then ybreit
    // __compulsory__ recomputing xrand every time!
    while (yrand > ybreit ) {
        xrand = uniform.rand(xmin, xmax) ;
        yrand = uniform.rand(ymin, ymax) ;
        ybreit = BreitWigner( xrand, peak , hwhm) ;
    }
    return xrand ;
}

double prngGen::BreitWignerRel(double x, double peak , double hwhm){
    double gamma = sqrt( peak*peak * ( peak*peak + hwhm*hwhm ) ) ;
    double k = ( 2*sqrt(2) * peak * hwhm * gamma) 
        / (pi * sqrt( peak*peak + gamma )) ;
    return k / ( ( x*x - peak*peak ) * ( x*x - peak*peak ) 
        + peak*peak * hwhm*hwhm ) ;
}

double prngGen::BreitWignerRelTAC (double peak , double hwhm, 
        double xmin, double xmax, double ymin, double ymax ){
    double xrand = uniform.rand(xmin, xmax) ;
    double yrand = uniform.rand(ymin, ymax) ;
    double ybreit = BreitWignerRel( xrand, peak , hwhm) ;
    // accept xrand only if yrand is <= then ybreit
    // __compulsory__ recomputing xrand every time!
    while (yrand > ybreit ) {
        xrand = uniform.rand(xmin, xmax) ;
        yrand = uniform.rand(ymin, ymax) ;
        ybreit = BreitWignerRel( xrand, peak , hwhm) ;
    }
    return xrand ;
}