#include <iostream>
#include <string>
#include <bitset>
#include <cstdio>
#include "prngXorshift1024.h"

int main(int numArg, char* listArg[]) {
    prngXorshift1024 generator;
    randtype randomnumber = generator.xorshift1024star();
    std::cout.write(reinterpret_cast<char*>(&randomnumber), sizeof randomnumber) ;

    while(1) {
        randomnumber = generator.xorshift1024star(); 
        std::cout.write(reinterpret_cast<char*>(&randomnumber), sizeof randomnumber);
    }

    return 0;
}
