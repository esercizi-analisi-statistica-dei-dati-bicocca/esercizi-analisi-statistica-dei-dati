\documentclass[relazione.tex]{subfiles}

\begin{document}

\chapter{Secondo Esercizio}

Lo scopo di questo esercizio è calcolare numericamente la costante $\gamma$ di Eulero-Mascheroni, analizzando le varie sorgenti di errore.

La costante è definita come: 
\[
\gamma = \lim_{n \rightarrow \infty } \left( \sum_{k=1}^n \frac1{k} - \ln n \right) \simeq 0,57721 56649 01532 86060 65120 90082
\]

si riconosce la presenza nella definizione dell'ennesimo numero armonico 
\[
	H_n = \sum_{k=1}^n \frac1{k}
\]

\section{Sorgenti di errore}
In generale si intende studiare un problema fisico, la cui soluzione viene indicata con $x_f$, attraverso dei metodi numerici che portano al risultato $\hat{x}_n$, seguendo la descrizione fornita in \cite{Quarteroni2014}. 

L'errore totale $e = x_f - \hat{x}_n$ può essere separato in errore del modello matematico $e_m$ e errore computazionale $e_c = x - \hat{x}_n$, con $F(x,d)=0$ problema analitico che modellizza il problema fisico, quindi $e = e_m+e_c$.

A sua volta l'errore computazionale $e_c$ risulta la combinazione dell'errore di discretizzazione numerica $e_n$, detto anche errore analitico, legato al fatto che un problema può essere calcolato numericamente solo se è descritto da un algoritmo finito che risolve il problema approssimato $F_n(x_n,d_n)=0$, quindi ad esempio una serie deve essere troncata se la si vuole approssimare, e di errore di arrotondamento o \emph{round-off} $e_a$, legato alla rappresentazione finita dei numeri all'interno di un calcolatore. si ha che $e_c = e_m + e_a$ .

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/Screenshot_20180516_165416}
	\caption{Schema delle sorgenti di errore \cite{Quarteroni2014}}
	\label{fig:screenshot20180516165416}
\end{figure}

Un metodo numerico è descritto dai seguenti parametri:
\begin{itemize}
	\item stabilità del problema: piccole perturbazioni sui dati iniziali danno luogo a perturbazioni dello stesso ordine di grandezza sulla soluzione. È stimata dal numero di condizionamento. È definita sia per il problema $F$ che per il problema $F_n$.
	\item accuratezza: errori piccoli rispetto a una tolleranza fissata, stimato dall'ordine di infinitesimo di $e_n$ rispetto a un parametro caratteristico della discretizzazione. Non è in teoria limitato dalla rappresentazione dei numeri nel calcolatore
	\item precisione: parametro legato al sistema numerico usato per rappresentare i numeri nel calcolatore.
\end{itemize}

Nel problema che si affronta in questo esercizio $e_m$ non è definito, essendo un problema puramente matematico, quindi le uniche sorgenti di errore sono la precisione finita e la discretizzazione numerica.


\subsection{Rappresentiazione dei numeri sul calcolatore}
La rappresentazione finita dei numeri su un calcolatore comporta un errore detto di arrotondamento o round-off, dal momento che solo un sottoinsieme dell'insieme dei reali può essere rappresentato.

\subsubsection{Sistema posizionale}
Fissata una base $\beta \in \mathrm{N}, \ \beta \geq 2$, un numero reale con un numero finito di cifre $x_k$, con $k = -m, ... , n$ è rappresentato convenzionalmente con 

\[
x_{\beta} = (-1)^s [x_n x_{n-1} ... x_1 x_0 . x_{-1} x_{-2} ... x_{-m}] = (-1)^s \sum_{m=-m}^{n}x_k \beta^k
\]

con $x_n \neq 0$ e $s=0$ se x è positivo, $s=1$ se è negativo.

\subsubsection{Virgola fissa}

Il modo più intuitivo per salvare un numero reale su un calcolatore in N celle di memoria quindi è avere $N-k-1$ celle per le cifre intere e k per le cifre oltre il punto e una per il segno:

\[x = (-1)^s [x_{N-2} ... x_k . x_{k-1} ... x_{0}]\]

Lo svantaggio è che il numero massimo e minimo rappresentabili sono limitati con questa rappresentazione, a meno di usnare grandi regioni di memoria.

\subsubsection{Virgola mobile}

si può rappresentare un numero reale $x$ con 
\[
x = (-1)^s [0.x_1 ... x_t] \beta ^e = (-1)^s m \beta^{e-t}
\]
con $t$ numero cifre significative, $x_k$ cifra tale che $0 \leq x_k \leq \beta-1$, $m=[x_1 x_2 ... x_t]$ numero intero detto mantissa tale che $0 \leq m \leq \beta^t-1$, e $e$ numero intero detto esponente tale che $L \leq e \leq U$.

L'insieme dei numeri macchina rappresentati come qui appena descritto viene denotato con $\mathrm{F}(\beta, t, L, U)$.

Per avere unicità nella rappresentazione si suppone che $x_1 \neq 0$, quindi si ha che $x \geq \beta^{t-1}$, quindi $\beta^{t-1} \leq m \leq \beta^t-1 $.

\subsubsection{IEEE 754}

Lo standard \emph{IEEE 754-2008} per la rappresentazione dei numeri in virgola mobile fissa alcuni vincoli sui valori $\beta, t, L, U$, per evitare proliferazione di formati diversi. Ad esempio:

\begin{tabular}{ c | c | c | c | c | c | c | c }
nome & nome comune & base & t, \emph{digits} & L & U & dec digits & dec exp max \\
\hline
binary32 & single precision & 2 & 24 & -126 & 127 & 7.22 & 38.23 \\
binary64 & double precision & 2 & 53 & -1022 & 1023 & 15.95 & 307.95 
\end{tabular}

Si osserva inoltre che lo standard impone la risoluzione dell'ambiguità di rappresentazione di un numero reale, in particolare nei sistemi con $\beta = 2$ si ha che $x_1 = 1$, e quindi non è necessario che venga usata una cella di memoria per salvarlo. il numero di bit è quindi così ripartito:

\begin{tabular}{ c | c | c | c }
	nome & bit mantissa & bit esponente & bit segno \\
	\hline
	binary32 & 23 & 8 & 1 \\
	binary64 & 52 & 11 & 1
\end{tabular}

Inoltre lo standard prevede la presenza di numeri denormalizzati, che permette di aumentare la precisione e permettere che esistano dei numeri la cui cifra più significativa ha un esponente minore di $L$. In questo caso i bit dell'esponente sono impostati al minor valore possibile, e la mantissa non sottointende più che ci sia una cifra diversa da zero non salvata in memoria. Inoltre lo standard prevede la presenza di infiniti e not a number, e di due zeri, uno per segno.

\begin{tabular}{ c || c | c  }
	categoria & valore esponente (valore scritto in memoria) & valore mantissa \\
	\hline
	zeri & -127 (0) & 0 \\
	numeri denormalizzati & -127 (0) & qualunque \\
	numeri normalizzati & [-126,127] ([1, 254]) & qualunque \\
	infiniti & 127 (255) & 0 \\
	Nan & 127 (255) & qualunque
\end{tabular}


\section {Calcolo della costante di Eulero-Mascheroni}

\subsection{Descrizione algoritmo}

L'algoritmo per il calcolo della costante è separato in due step. Il primo è il calcolo della somma, che è implementato in una classe templata di C++, ed è usato con diverse tipologie di rappresentazione di numeri decimali per salvare il valore parziale della somma a ogni step. In particolare sono usati:

\begin{itemize}
	\item float, con indici int
	\item double, con indici int e long in
	\item long double, con indici int e long int
	\item precisione arbitraria default delle boost/multiprecision
	\item precisione arbitraria con GMP (GNU MultiPrecision)
	\item precisione arbitraria con GNU MPFR  (GNU Multiple Precision Floating-Point Reliably)
\end{itemize}

Il secondo step è quello della sottrazione del logaritmo, e questo è fatto in una classe che specifica.

Implementazione della somma, con \emph{template < typename myfloat, typename myint >
} preambolo della classe:

\begin{minted}{C}
myfloat directSum(myint n = 1000){
    sum = 0;
    for (myint k=1; k <= n; k++){
        sum += 1. / (myfloat) k ;
    }
return sum;
} ;
\end{minted}

Implementazione della costante per float e long double, si noti l'uso di \emph{logl} per i long double:

\begin{minted}{C}
float emConst::emConstFloatInt(int n){
  Sum<float, int> sum ;
  return sum.directSum(n) - log( (float) n) ;
}

long double emConst::emConstLongdoubleLong(long int n){
  Sum<long double, long int> sum ;
  return sum.directSum(n) - logl( (long double) n) ;
}
\end{minted}

\subsection{Analisi dei risultati ottenuti}

Si comincia analizzando l'andamento del valore della costante con float-int
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{../es2/analysis/analysis_files/analysis_4_0}
	\caption{Si nota che per circa 5e5 il valore della costante inizia a oscillare}
	\label{fig:analysis30}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{../es2/analysis/analysis_files/analysis_4_1}
	\caption{Si nota che per circa 5e5 il valore della costante inizia a oscillare, per poi divergere esponenzialmente}
	\label{fig:analysis31}
\end{figure}

Si usa quindi  l'algoritmo della somma di Kahan, \cite{Quarteroni2014} esercizio 14 capitolo 2. Questo algoritmo tiene traccia dell'errore che viene commesso a ogni somma, e aggiorna di conseguenza il valore della somma. Fare attenzione a usare flag del compilatore troppo \emph{aggressive}, che sfruttano la proprietà associativa della somma per ridurre il numero di operazioni da compiere.

\begin{minted}{c}
myfloat KahanSum(myint n = 1000){
  sum = 0;
  c = 0.;
  for (myint k=1; k <=n; k++){
    myfloat y = 1. / ( (myfloat) k) - c ;
    myfloat t = sum + y ;
    c = (t - sum) - y ;
    sum = t ;
  }
  return sum;
} ;
\end{minted}

utilizzato in questo modo:
\begin{minted}{c}
double emConst::emConstDoubleIntKahan(int n){
  Sum<double, int> sum ;
  return sum.KahanSum(n) - log( (double) n) ;
}
\end{minted}

Si osserva che questo algoritmo garantisce la non esplosione della somma, e a alti n il valore di gamma ottenuto oscilla attorno al valore vero.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{../es2/analysis/analysis_files/analysis_5_0}
	\caption{}
	\label{fig:analysis40}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{../es2/analysis/analysis_files/analysis_5_1}
	\caption{}
	\label{fig:analysis41}
\end{figure}

Si osserva che $2^{31} = 2147483648$, quindi se si usano degli indici \emph{int} non si può andare oltre. per superare questo limite bisogna usare i \emph{long int}, che permettono di arrivare a circa $9e18$. Si osservi che non si usano indici unsigned, visto che permettono un guadagno di circa un fattore due, che non è molto soddisfacente quando si studiano le caratteristiche di un algoritmo su diversi ordini di grandezza.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{../es2/analysis/analysis_files/analysis_8_0}
	\caption{}
	\label{fig:analysis70}
\end{figure}

Possiamo quindi comparare l'andamento dell'errore relativo ottenuto con i vari metodi:

\begin{figure}[H]
	\centering
	\includegraphics[width=1\linewidth]{../es2/analysis/analysis_files/analysis_11_0}
	\caption{errore relativo, ottenuto con $\frac{\gamma_n-\gamma}{\gamma}$}
	\label{fig:analysis110}
\end{figure}

Si osserva che usando i float e la somma di Kahan l'errore relativo si assesta intorno a 0.5e-6, che è il massimo che si può ottenere teoricamente, visto che la somma è circa uno e che i float garantiscono 7 digits decimali.

Si osserva inoltre che per circa 1e10 l'errore nei double-long inizia ad accumularsi e a crescere esponenzialmente, mentre per i double-long con la somma di Kahan questo effetto non è presente e la somma continua a convergere verso il valore vero fino al massimo n usato. Lo stesso andamento è presente con i long double-long.

Sia per i double-int che per i double-int con somma di Kahan la somma conevrge fino al massimo n osservato, visto che è minore del valore a cui si inizia ad accumulare con i double-long. 

\subsection{Tempo di esecuzione}

Si studia il costo computazionale, espresso in termini di tempo, che ha richiesto ogni punto. 
 
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../es2/analysis/analysis_files/analysis_12_0}
	\caption{}
	\label{fig:analysis120}
\end{figure}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../es2/analysis/analysis_files/analysis_12_1}
	\caption{}
	\label{fig:analysis121}
\end{figure}

I tempi di esecuzione sono lineari nel tempo, infatti il costo della somma è $O(n)$. 
Utilizzare la somma di Kahan rallenta l'esecuzione, al punto che risulta conveniente aumentare la precisione delle variabili, piuttosto che cambiare l'algoritmo di somma. Inoltre usare i long int al posto degli in non rallenta l'esecuzione della somma diretta (double-int e double-float impiegano circa lo stesso tempo), mentre la rallenta nel caso della somma di Kahan (double-long è più lengo di double-int quando si usa la somma di Kahan).

Tuttavia, usare i \emph{long double} non comporta un grande vantaggio, dal momento che nel sistema in cui si è svolto l'esercizio queste sono le caratteristiche dei double e double long:
\begin{minted}{latex}
double:
min norm:    2.22507e-308
max norm:    1.79769e+308
epsilon:     2.22045e-16
dig:         15
mant dig:    53
exp min bin: -1021
exp max bin: 1024
exp min dec: -307
exp max dec: 308

long double:
min norm:    3.3621e-4932
max norm:    1.18973e+4932
epsilon:     1.0842e-19
dig:         18
mant dig:    64
exp min bin: -16381
exp max bin: 16384
exp min dec: -4931
exp max dec: 4932
\end{minted}

Con i long double si guadagnano solo tre cifre decimali rispetto ai double. 

\subsection{Precisione arbitraria}

Se si vuole calcolare la costante di eulero-mascheroni con precisione arbitraria, si devono usare delle librerie che permettono di impostare il numero di digits esatte desiderate. Il tempo di esecuzione con queste librerie, però, aumenta considerevolmente. Si confrontano i tempi di esecuzione con $n=5e7$ e che garantissero almeno 50 cifre decimali esatte, non tanto perchè si prevede che vengano tutte calcolate esattamente (il costo in termini di tempo sarebbe proibitivo), bensì per valutare quanto costa quadruplicare circa il numero di bit assegnati alla mantissa.

\begin{tabular}{ c | c | c | c }
	Libreria& bit mantissa (digits) & cifre decimali esatte & tempo esecuzione (s) \\
	\hline
	Boost - default    & 257 & 77 &  77 \\
	GMP                & 256 & 77 & 3.5 \\
	GMP - boost        & 257 & 77 & 4.1\\
	MPFR - boost       & 257 & 77 & 4.7 \\
	\hline
    double-long        & 53 & 15 & 0.167058 \\
    long double - long & 64 & 18 & 0.268795 \\
    double-int         & 53 & 15 & 0.346920 \\
    double-int, kahan  & 53 & 15 & 0.358115 \\
    double-long, kahan & 53 & 15 & 0.519151 \\
    float-int, kahan   & 23 & 7  & 0.653408 \\
\end{tabular}

Aumentare di circa 4 volte la dimensione della mantissa costa un'ordine di grandezza circa in termini di tempo di esecuzione.

\subsection{Listings}

Classe templata per la somma: sum.h
\inputminted[]{c}{../es2/sum/sum.h}

Classe che implementa il calcolo della costante di eulero-mascheroni: emconst.h
\inputminted[]{c}{../es2/emconst/emconst.h}
Implementazione: emconst.cpp
\inputminted[]{c}{../es2/emconst/emconst.cpp}

Classe templata ausiliaria per avere dei range distribuiti logaritmicamente: range.h
\inputminted[]{c}{../es2/range/range.h}

Esempi di main: double-int con somma di Kahan
\inputminted[]{c}{../es2/emDoubleIntKahan.cpp}
long double-long con somma diretta
\inputminted[]{c}{../es2/emLongDoubleLong.cpp}

Informazioni relative alla propria architettura sui numeri float:
\inputminted[]{c}{../es2/limits_summary.cpp}

Precisione arbitraria:
Boost
\inputminted[]{c}{../es2/arbitrary-precision/emBoost.cpp}
GMP
\inputminted[]{c}{../es2/arbitrary-precision/emGmp.cpp}
GMP - Boost
\inputminted[]{c}{../es2/arbitrary-precision/emGmp1.cpp}
MPFR - Boost
\inputminted[]{c}{../es2/arbitrary-precision/emMpfr.cpp}
\end{document}