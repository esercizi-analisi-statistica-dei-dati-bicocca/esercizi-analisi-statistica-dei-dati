/*

This requires the libraries:
* boost
* the mpfr (libmpfr-dev)
* gmp

*/

#include <boost/multiprecision/gmp.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <boost/math/constants/constants.hpp>

typedef boost::multiprecision::
    number<boost::multiprecision::gmp_float<77> > myfloat;

int main()
{
    // this work only with fixed precision
    std::cout << std::numeric_limits<myfloat>::digits << std::endl;
    std::cout << std::numeric_limits<myfloat>::digits10 << std::endl;

    // Operations at fixed precision and full numeric_limits support:
    myfloat sum = 0.;
    myfloat result = 0.;
    myfloat residual = 0.;

    long int n = 50000000 ;
    for (long int i=1; i<n; i++){
        sum += 1./(double)i ;
    }
    result = sum - log( (myfloat) n);
    residual = boost::math::constants::euler<myfloat>() - result;

    std::cout 
        << std::setprecision(std::numeric_limits<myfloat>::max_digits10)
        << result << "," << residual 
        << std::endl;
    return 0;
}