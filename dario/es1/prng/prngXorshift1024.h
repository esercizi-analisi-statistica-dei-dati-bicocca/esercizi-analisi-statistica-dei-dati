#ifndef prngXorshift1024_H
#define prngXorshift1024_H

#include "prngLcg.h"

typedef unsigned long long int randtype;

class prngXorshift1024 {
    private:
    prngLcg uniform ;
    int a ;
    int b ;
    int c ;
    int p ;
    randtype s[16] ;

    public:
    prngXorshift1024();
    ~prngXorshift1024();
    randtype xorshift1024star(); 
    double rand(double min=0., double max=1.); 
};

#endif //prngXorshift1024_H