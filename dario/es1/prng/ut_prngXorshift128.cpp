#include <iostream>
#include <string>
#include <bitset>
#include "prngXorshift128.h"

int main(int numArg, char* listArg[]) {
    
    int N = 10;
    if (numArg > 1){
        N = std::stoi( listArg[1] );
    }

    prngXorshift128 generator;
    for (int i=0; i<N; i++){
        // std::cout << generator.randInt() << std::endl;
        std::cout << generator.rand(-10, 10) << std::endl;
    }

    return 0;
}