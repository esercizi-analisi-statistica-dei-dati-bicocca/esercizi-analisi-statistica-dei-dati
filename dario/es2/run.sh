#!/bin/bash

set -x

MAXFLOAT=100000000
MAXDOUBLE=1 000 000 000 000
MAXQUAD=1 000 000 000 000 000 000

# time ./build/emconst-float 10 $MAXFLOAT 10 direct > data/direct-float.txt
# time ./build/emconst-float 10 $MAXFLOAT 10 inverse > data/inverse-float.txt
time ./build/emconst 10 $MAXDOUBLE 10 direct > data/direct.txt
time ./build/emconst 10 $MAXDOUBLE 10 inverse > data/inverse.txt
time ./build/emconst-test 10 $MAXDOUBLE 10 direct > data/direct-test.txt
time ./build/emconst-test 10 $MAXDOUBLE 10 inverse > data/inverse-test.txt
time ./build/emconst-quad 10 $MAXQUAD 10 direct > data/direct-quad.txt
time ./build/emconst-quad 10 $MAXQUAD 10 inverse > data/inverse-quad.txt
