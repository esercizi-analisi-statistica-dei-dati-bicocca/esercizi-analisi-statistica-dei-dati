\documentclass[Relazione.tex]{subfiles}


\begin{document}
\hypertarget{esercizio-2}{%
	\section{Esercizio 2}\label{esercizio-2}}

In questo esercizio sono stati confrontati diversi algoritmi per il calcolo numerico della costante di Eulero-Mascheroni $\gamma$, discutendo l'influenza degli errori nel calcolo. 

\begin{equation}
\gamma = \lim_{n \rightarrow +\infty} \left( \sum_{k=1}^{n} \frac{1}{k} - \log (n)\right) = 0.577215664901532860606512090082402431042159....
\end{equation}

\subsection{Gli errori di calcolo}
Un calcolatore rappresenta i numeri in memoria e durante i calcoli in forma binaria. Lo standard IEEE 754 descrive la rappresentazione \emph{floating point} dei numeri attualmente più diffusa (ma ne esistono altre, ad esempio l'aritmetica decimale). Un numero viene rappresentato attraverso un segno, un esponente e una mantissa (contenente le sue cifre significative). 
\[x = \pm d \cdot 2^\alpha  \]
L'esponente $\alpha$ è memorizzato in 8 o 11 bits, la mantissa in 24 o 53 a seconda che si utilizzi il formato a singola-precisione (32 bit, C \verb+float+) o a doppia-precisione (64 bits, C \verb+double+), corrispondenti a un numero di cifre decimali significative rispettivamente di 7 e 16. 

Il calcolatore quindi dispone di una aritmetica finita: è in grado di memorizzare un numero finito di numeri reali all'interno di un certo range fissato e se il risultato di un'operazione non è un numero direttamente rappresentabile deve essere approssimato al numero più vicino. Questo fatto introduce i cosidetti \emph{errori di round-off} nei calcoli numerici. 

Distinguiamo due categoria di errori che affliggono il calcolo della costante $\gamma$: 
\begin{description}
	\item[Errori analitici] Non è possibile sommare all'infinito tutti gli elementi della serie che definisce la costante, è necessario fermarsi dopo un numero prestabilito di elementi successivi. Questo fatto introduce l'errore analitico nel calcolo della serie. 
	\item[Errori algoritmici] Poichè l'aritmetica di un calcolatore è finita i calcoli sono soggetti a errori di round-off. Diversi algoritmi possono portare a diversi errori: infatti alcuni possono essere instabili cioè accumulare errori di round-off fino a portare a un risultato completamente sbagliato. La scelta dell'algoritmo migliore permette di minimizzare il cosidetto errore algoritmico. 
\end{description}

\subsection{Calcolo numerico della costante}
Per il calcolo della costante di Eulero si è prima di tutto confrontato l'utilizzo di numeri \verb+float+ e \verb+double+, che possiedono una diversa precisione intrinseca (numero di cifre significative memorizzabili). Poi si sono confrontati due algoritmi di calcolo con lo scopo di minimizzare l'errore algoritmico. 
\begin{figure}
	\centering
	\adjustimage{max size={0.8\linewidth}{0.8\paperheight}}{images/es2/euler_constant}
	\caption{Stima dell'errore nel calcolo della costante di Eulero}
	\label{eulero_error}
\end{figure}
Come si può osservare nel plot dell'errore relativo nel calcolo della costante per diversi ordini di grandezza di iterazione (vedi fig. \vref{eulero_error}), il calcolo eseguito con i \verb+float+ diviene instabile quando si raggiunge una precisione di $10^{-6}$ (questo è coerente con le 7 cifre significative memorizzabili in un \verb+float+), dopo circa $10^4$ iterazioni: con un numero maggiore di iterazione gli errori di round-off rendono instabile il calcolo. 
L'utilizzo dei \verb+double+ permette invece di essere limitati solamente dall'errore analitico, raggiungendo una precisione di $10^{-9}$ in $10^8$ iterazioni, senza accumulare errori di round-off.

Sono stati studiati gli algoritmi di Kahan e Pairwise summation per ridurre gli errori di round-off nell'utilizzo dei float. 
\subsubsection{Kahan summation}
Poichè gli errori di round-off provengono dal fatto che vengono perse cifre significative nella somma tra un numero grande e uno piccolo, ad ogni iterazione nel calcolo della serie viene salvato in una variabile questo scarto, che è poi sommato nell'iterazione successiva. 

\begin{minted}[linenos]{c++}
float euler_float_kahan(int n){
	float sum = 0.;
	float c = 0.;
	for (int i = 1; i<=n; i++){
		float y = (1/(double)i) - c ;
		float t =  sum + y ;
		// Si estrae lo scarto
		c = (t - sum)  - y;
		sum = t;
	}
	sum -= log(n);    
	return sum;
}
\end{minted}
Come si osserva nel grafico \vref{eulero_error}, l'utilizzo dell'algoritmo di Kahan permette di utilizzare i \verb+float+ fino alla precisione di $10^{-7}$, il massimo possibile.

\subsubsection{Pairwise summation}
La tecnica della Pairwise summation, o somma a cascata, permette di ridurre l'errore di round-off accumulato nella semplice somma in sequenza di tutti gli addendi della serie. In generale questa tecnica è meno efficiente della Kahan summation, ma ha un costo computazionale molto basso, quasi equivalente a quello della somma semplice. 
La tecnica prevede di suddividere ricorsivamente la sequenza da sommare in due parti, fino a raggiungere un numero $N$ (si è utilizzato $N=100$) di elementi per ogni partizione. A questo punto si sommano gli elementi di ogni gruppo e si ricostruisce la somma finale. 

\begin{minted}[linenos]{c++}
float pairwise_sum(int nstart, int nstop){
    if ((nstop - nstart ) < 100){
        float sum = 0.;
        for (int i = nstart; i<=nstop; i++){
            sum += 1 / (double)i;
        }
        return sum;
    }else{
        int m = floor((nstop - nstart) /2 );
        // Si divide ricorsivamente la somma
        return pairwise_sum(nstart, nstart+m) + pairwise_sum(nstart+m+1, nstop);
    }
}

float euler_pairwise(int n){
    float psum = pairwise_sum(1, n);
    return psum - log(n);
}
\end{minted}
Come si osserva nel grafico \vref{eulero_error}, questa tecnica limita gli errori di round-off, non raggiungendo però le performance della Kaham summation. 

\subsection{Listato completo}

\begin{minted}[
frame=lines,
framesep=2mm,
fontsize=\footnotesize,
baselinestretch=0.8,
linenos]{c++}
#include <iostream>
#include <cmath>
#include <numeric>
#include <limits>
#include <iomanip>
#include <fstream>
#include <gsl/gsl_math.h>
#include "TGraph.h"
#include "TCanvas.h"
#include "TMultiGraph.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLegend.h"
using namespace std;

double euler_double(int n){
    double sum = 0.;
    for (int i = 1; i<=n; i++){
        sum += 1 / (double)i;
    } 
    sum -= log(n);
    return sum;
}
float euler_float(int n){
    float sum = 0.;
    for (int i = 1; i<=n; i++){
        sum += 1 / (double)i;
    }
    sum -= log(n);
    return sum;
}
float euler_float_kahan(int n){
    float sum = 0.;
    float c = 0.;
    for (int i = 1; i<=n; i++){
        float y = (1/(double)i) - c ;
        float t =  sum + y ;
        c = (t - sum)  - y;
        sum = t;
    }
    sum -= log(n);    
    return sum;
}
float pairwise_sum(int nstart, int nstop){
    if ((nstop - nstart ) < 100){
        float sum = 0.;
        for (int i = nstart; i<=nstop; i++){
            sum += 1 / (double)i;
        }
        return sum;
    }else{
        int m = floor((nstop - nstart) /2 );
        return pairwise_sum(nstart, nstart+m) + pairwise_sum(nstart+m+1, nstop);
    }
}
float euler_pairwise(int n){
    float psum = pairwise_sum(1, n);
    return psum - log(n);
}

int main(){
    cout << "Euler constant: " << setprecision(numeric_limits<long double>::digits10 +1) 
    		<< M_EULER << endl;
    
    double * results_double = new double[16];
    double * results_float = new double[16];
    double * results_float_kahan = new double[16];
    double * results_float_pairwise = new double[16];
    double * xs = new double[16];
 
    for (int i = 2; i <= 18; i++){
        double x = pow(10, (double)i/2);
        xs[i-2] = x;
	    results_double[i-2] = abs(euler_double(x) - M_EULER);
        results_float[i-2] = abs( euler_float(x) - M_EULER);
        results_float_kahan[i-2] = abs(euler_float_kahan(x) - M_EULER);
        results_float_pairwise[i-2] = abs(euler_pairwise(x) - M_EULER);
    }

    gStyle->SetPalette(kBlueGreenYellow);
    TCanvas * c = new TCanvas("canvas", "Euler constant estimation", 800, 600);
    TLegend * leg  = new TLegend();
    TGraph * g_double = new TGraph(16, xs, results_double);    
    TGraph * g_float = new TGraph(16, xs, results_float);
    TGraph * g_float_kahan = new TGraph(16, xs, results_float_kahan);
    TGraph * g_float_pairwise = new TGraph(16, xs, results_float_pairwise);
    leg->AddEntry(g_double, "double", "lp");
    leg->AddEntry(g_float, "float", "lp");
    leg->AddEntry(g_float_kahan, "float_kahan", "lp");
    leg->AddEntry(g_float_pairwise, "float_pairwise", "lp");
    TMultiGraph * mg = new TMultiGraph();
    mg->SetTitle("Euler constant estimation;N iterations;#gamma euler - #gamma");
    mg->Add(g_double);
    mg->Add(g_float);
    mg->Add(g_float_kahan);
    mg->Add(g_float_pairwise);
    mg->Draw("AC* PMC PLC");
    leg->Draw("same");
    c->SetLogx();
    c->SetLogy();
    c->SaveAs("euler_constant.pdf");
}
\end{minted}

\end{document}