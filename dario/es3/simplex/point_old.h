#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <vector>

template <typename myfloat>
class Point {
    public:
    // Point();
    Point(int newdim);
    Point(int component, myfloat value, Point<myfloat> p0);
    // Point(int newdim, int component, myfloat value);
    Point(const Point& p);
    Point(std::vector<myfloat> p);
    ~Point(){};
    void setPoint(std::vector<myfloat> p);
    // std::vector<myfloat> getPoint();
    int getDim() {return dim;} ;
    void scale(myfloat scalefactor);
    void printPoint();
    Point& operator= (const Point& p);
    Point operator+ (const Point& p);
    Point operator- (const Point& p);
    Point operator* (myfloat scale);
    myfloat& operator[] (int component);


    private:
    int dim;
    std::vector<myfloat> point;
};

// template <typename myfloat>
// Point<myfloat>::Point(){
//     point.clear();
//     dim = 2;
//     for (int i=0; i<dim; i++){
//         point.push_back(0.);
//     }
// }

template <typename myfloat>
Point<myfloat>::Point(int newdim){
    point.clear();
    dim = newdim;
    for (int i=0; i<dim; i++){
        point.push_back(0.);
    }
}

template <typename myfloat>
Point<myfloat>::Point(int component, myfloat value, Point<myfloat> p0){
    point.clear();
    dim = p0.getDim() ;
    std::cout << dim << std::endl ;
    for (int i=0; i<dim; i++){
        // double xi = p0.point[i] ;
        double xi = p0[i] ;
        if (i == component){
            xi += value;
        }
        point.push_back(xi);
    }
}

template <typename myfloat>
Point<myfloat>::Point(const Point& p){
    point.clear();
    std::vector<myfloat> p0 = p.point;
    for (auto it = p0.begin(); it != p0.end(); it++ ){
        point.push_back(*it) ;
    }
}

template <typename myfloat>
Point<myfloat>::Point(std::vector<myfloat> p){
    point.clear();
    dim=0;
    for (auto it = p.begin(); it != p.end(); it++ ){
        point.push_back(*it) ;
        dim++ ;
    }
};

template <typename myfloat>
void Point<myfloat>::setPoint(std::vector<myfloat> p){
    point.clear();
    dim=0;
    for (auto it = p.begin(); it != p.end(); it++ ){
        point.push_back(*it) ;
        dim++ ;
    }
};

template <typename myfloat>
void Point<myfloat>::scale(myfloat scalefactor){
    for (auto it = point.begin(); it != point.end(); it++ ){
        *it = *it * scalefactor ;
    }
};

template <typename myfloat>
void Point<myfloat>::printPoint(){
    std::cout << " ( " ;
    for (auto it = point.begin(); it != point.end(); it++ ){
        std::cout << *it << " " ;
    }
    std::cout << ")" << std::endl;
};

template <typename myfloat>
Point<myfloat>& Point<myfloat>::operator=(const Point& p){
    point.clear();
    // for (auto it = p.point.begin(); it != p.point.end(); it++ ){
    //     point.push_back(*it) ;
    // }
    dim = p.point.size();
    for (int i=0; i<dim; i++ ){
        point.push_back(p.point[i]);
    }
    return *this;
};

template <typename myfloat>
Point<myfloat> Point<myfloat>::operator+(const Point& p){
    std::vector<myfloat> sum;
    // std::vector<myfloat> addend;
    // addend = p.getPoint();
    for (int i=0; i<point.size(); i++ ){
        sum.push_back( point[i] + p.point[i]) ;
    }
    Point<myfloat> sumpoint(sum);
    return sumpoint;
};

template <typename myfloat>
Point<myfloat> Point<myfloat>::operator-(const Point& p){
    std::vector<myfloat> diff;
    for (int i=0; i<point.size(); i++ ){
        diff.push_back( point[i] - p.point[i]) ;
    }
    Point<myfloat> diffpoint(diff);
    return diffpoint;
};

template <typename myfloat>
Point<myfloat> Point<myfloat>::operator*(myfloat scale){
    std::vector<myfloat> scaled;
    for (int i=0; i<point.size(); i++ ){
        scaled.push_back( point[i] * scale) ;
    }
    Point<myfloat> scaledpoint (scaled);
    return scaledpoint;
};

// template <typename myfloat>
// myfloat Point<myfloat>::operator[](int component){
//     return point.at(component);
// };
template <typename myfloat>
myfloat& Point<myfloat>::operator[](int component){
    return point[component];
};


#endif //POINT_H