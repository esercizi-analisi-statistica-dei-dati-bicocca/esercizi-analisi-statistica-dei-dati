#!/bin/bash

rm -rf build
rm -rf install
mkdir build
mkdir install
mkdir data

CURRENT_PATH=$PWD

cd build
cmake -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_INSTALL_PREFIX="$CURRENT_PATH/install" ..
make
# make install
cd ..
