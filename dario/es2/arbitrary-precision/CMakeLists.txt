cmake_minimum_required(VERSION 2.6)
project(arbitrary-precision)

# gmpxx may be removed
add_executable(emGmp emGmp.cpp)    
target_link_libraries(emGmp gmpxx gmp)

# gmpxx may be removed
add_executable(emGmp1 emGmp1.cpp)    
target_link_libraries(emGmp1 gmpxx gmp)

add_executable(emBoost emBoost.cpp)

add_executable(emMpfr emMpfr.cpp)
target_link_libraries(emMpfr mpfr gmp)

install (TARGETS emGmp emGmp1 emBoost emMpfr DESTINATION bin)
