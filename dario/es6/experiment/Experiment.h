#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include "prngXoroshiro128.h"
#include <iostream>
#include <cmath>

class Experiment {
    public:
    Experiment( int experimentSize, double smearingFactor,
        double newlambda, int slits,
        double slitsize, double slitdistance, double screenDistance);
    ~Experiment();
    double  spettro(double x);
    double* sample_hit_or_miss(double xmin, double xmax );
    double* smearing(double xmin, double xmax, int bins );

    private:
    int Nfend ; // number of slices
    double L ; // distance from screen
    double A ; // size of a slice
    double D ; // distance from the slices
    double lambda ; // light wave lenght
    int Nsamp ; // size of the toy experiment
    double SMfactor ; // smearing factor

    prngXoroshiro128 rnd ;
    double* idealData ;
    double* smearedData ;
    // double realData[Nsamp] ;

};

#endif