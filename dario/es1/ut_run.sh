#!/bin/zsh

# ./build/prng/ut_prngLcg 100000 > data/uniform.txt

./build/prng/ut_prngGenInv 100000 > data/breit.txt
./build/prng/ut_prngGenTac 100000 > data/breittac.txt
./build/prng/ut_prngGenRelTac 100000 > data/breitreltac.txt

# ./build/prng/ut_prngXorshift128 10000 > data/xorshift128.txt
# ./build/prng/ut_prngXorshift1024 10000 > data/xorshift1024.txt
# ./build/prng/ut_prngXorshift128 10000 > data/xoroshiro128.txt