/*
g++ -o ut_sum ut_sum.cpp
*/

#include "sum.h"
#include <iostream>
#include <iomanip>
#include <limits>

int main (int numArg, char* listArg[]){

    int M = 100 ;
    Sum<float, int> shortsum ;
    std::cout << M << " "  
        << std::setprecision(std::numeric_limits<float>::digits10+1) 
        << shortsum.directSum(M)
        << std::endl;    
    std::cout << M << " "  
        << std::setprecision(std::numeric_limits<float>::digits10+1) 
        << shortsum.KahanSum(M)
        << std::endl;   

    long int N = 1000 ;
    Sum<long double, long int> longsum ;
    std::cout << N << " "  
        << std::setprecision(std::numeric_limits<long double>::digits10+1) 
        << longsum.directSum(N)
        << std::endl;
    std::cout << N << " "  
        << std::setprecision(std::numeric_limits<long double>::digits10+1) 
        << longsum.KahanSum(N)
        << std::endl;

    return 0;
}