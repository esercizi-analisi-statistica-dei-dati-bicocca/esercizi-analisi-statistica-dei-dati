#!/bin/bash

root -l -b -q 'analysis/spectra.C ("./data/CeBr3_Ba133.dat", 5, 0.1)'
root -l -b -q 'analysis/spectra.C ("./data/CeBr3_Co57.dat", 5, 0.12)'
root -l -b -q 'analysis/spectra.C ("./data/CeBr3_Cs137.dat", 15, 0.01)'
root -l -b -q 'analysis/spectra.C ("./data/CeBr3_Eu152.dat", 15, 0.01)'
root -l -b -q 'analysis/spectra.C ("./data/CeBr3_Na22.dat", 15, 0.1)'
root -l -b -q 'analysis/calibration.C ()'