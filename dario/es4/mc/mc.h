#ifndef MC_H
#define MC_H

#include <iostream>
#include <vector>
#include <cmath>

template <typename generator>
class mc {
    public:
    mc(int ninit){
        xmin = 0.;
        // xmax = 1.
        xmax = 2.;
        n = ninit;
        mcresult.integral = 0;
        mcresult.variance = 0;
    };
    ~mc(){};

    double f(double x){
        return x*x*x*x*(1.+x) ;
    };
    double g(double x){
        // return 5*x*x*x*x ;
        // return (5/32.)*x*x*x*x ;
        return (3./32.)*x*x*x*x*x ;
    };
    double Ginv(double x){
        // return pow(x,0.2);
        // return 2*pow(x,0.2);
        return 2*pow(x,1./6.);
    };

    void set_parameters(int newN){
        n = newN;
        mcresult.integral = 0;
        mcresult.variance = 0;
    };

    double f_mc_short(){
        double integral = 0;
        for (int i=0; i<n; i++){
            integral += f( (xmax-xmin) * gen.rand(1.,0.) + xmin );
        }
        return (xmax - xmin) / ( (double) n) * integral;
    };

    void f_mc (){
        double integral=0;
        double variance=0;
        std::vector<double> fx;
        for (int i=0; i<n; i++){
            double fxi = f( (xmax-xmin) * gen.rand(0., 1.) + xmin );
            fx.push_back( fxi );
            integral += fxi ;
        }   
        mcresult.integral = integral * (xmax - xmin) / ((double) n) ;
        for (int i=0; i<n; i++){
            double residual = (fx[i] - mcresult.integral);
            variance +=  residual * residual ;
        }
        mcresult.variance = variance / ((double) n) / ((double) n-1.);
    };

    void f_mc_impsamp () {
        double integral=0;
        double variance=0;
        std::vector<double> fxgx;
        for (int i=0; i<n; i++){
            // random is properly distributed into the range
            // thanks to the choice of xmin, xmax, g and Ginv
            double random = Ginv( gen.rand(0.,1.) ); // [0.,1.] IMPOOOOO
            double fxigxi = f(random) / g(random) ;
            fxgx.push_back(fxigxi);
            integral += fxigxi ;
        }
        mcresult.integral = integral / ((double) n) ; // * (xmax - xmin)
        for (int i=0; i<n; i++){
            double residual = fxgx[i] - mcresult.integral;
            variance += residual*residual;
        }
        mcresult.variance = variance / ((double) n) / ((double) n-1.);
    }

    double f_get_integral(){ return mcresult.integral; };
    double f_get_variance(){ return mcresult.variance; };
    double f_get_stdev(){ return sqrt(mcresult.variance); };

    private:
    struct result{
        double integral;
        double variance;
    } mcresult;
    double xmin;
    double xmax;
    int n;
    generator gen;

};

#endif //MC_H