#ifndef EMCONST_H
#define EMCONST_H

class emConst {
    public:
    emConst();
    ~emConst();
    float       emConstFloatInt        (int n = 1000) ;
    float       emConstFloatIntKahan   (int n = 1000) ;
    double      emConstDoubleInt       (int n = 1000) ;
    double      emConstDoubleIntKahan  (int n = 1000) ;
    double      emConstDoubleLong      (long int n = 1000) ;
    double      emConstDoubleLongKahan (long int n = 1000) ;
    long double emConstLongdoubleLong  (long int n = 1000) ;
};

#endif // EMCONST_H