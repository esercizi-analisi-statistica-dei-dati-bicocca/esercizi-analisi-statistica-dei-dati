/*
g++ -o emGmp emGmp.cpp -lgmpxx -lgmp

install the following packages:
libgmp10
libgmpxx4ldbl -> libgmp10
libgmp-dev -> libgmp10
           -> libgmpxx4ldbl

*/

#include <iostream>
#include <string>
#include <gmpxx.h>
#include <cmath>
#include <gsl/gsl_math.h>

int main () {

    mpf_class sum(1.5, 256);
    sum = 0.;
    long int n = 50000000 ;
    for (long int i=1; i<n; i++){
        sum += 1./(double)i ;
    }
    mpf_class em(sum);
    em = sum - log((double) n);
    mpf_class resid(sum);
    resid = em - M_EULER;
    mp_exp_t exp;
    std::string emdec = em.get_str(exp);
    mp_exp_t expres;
    std::string resdec = resid.get_str(expres);
    std::cout << em.get_prec() << ","
        << emdec << "," << exp << std::endl
        << resdec << "," << expres
        << std::endl ;

    return 0;
}