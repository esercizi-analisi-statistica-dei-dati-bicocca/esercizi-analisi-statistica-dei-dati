#include "prngXorshift128.h"
#include <climits>

prngXorshift128::prngXorshift128(){
    a = 23 ;
    b = 17 ;
    c = 26 ;
    s[0] = uniform.randInt() ;
    s[1] = uniform.randInt() ;
}

prngXorshift128::~prngXorshift128(){}

randtype prngXorshift128::xorshift128plus(){
    randtype x = s[0] ;
    randtype y = s[1] ;
    s[0] = y ;
    x ^= x << a ;
    s[1] = x ^ y ^ ( x >> b) ^ ( y << c ) ;
    return s[1] + y ;
}

double prngXorshift128::rand(double min, double max){
    return (double) (max - min) * ( (double) xorshift128plus() )
        / ( (double) ULLONG_MAX ) + min ;
}