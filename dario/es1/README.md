# ES1

1. Generate pseudo-random numbers distributed as a Breit-Wigner.
2. Study some tests used to evaluate the quality of a uniform generator.

## compile

Use `./compile.sh`

If you have vscode, then this has been configured to be triggered by crtl+shift+b

### run

Run the Unit tests
`./ut_run.sh`

Then, run the analysis over the files produced by the unit test
* `cd analysis`
* `./analysis.sh`

Run the `dieharder` test on the pseudo-random number uniform generator
* install the `dieharder` package
* `./build/prng/ut_prngLcgDieharder | dieharder -a -g 200`

//TODO 
Run the testu01 test suite:
* `??`

## Libraries

### prngLcg

This is a simple Linear Congruential Generator.
More informations here: `https://en.wikipedia.org/wiki/Linear_congruential_generator`
It uses the MMIX Parameters and is not portable, since it requires that the
compiling machine has `unsigned long long int` of size 64bit.
The module parameter `m` is not set in the code, since due to the architecture itself
the random numer `x` automatically overflows when reaching `2**64`

Test: 

* `./build/ut_prng 100 > data/test.txt`
* `gnuplot`
* `> plot "data/test.txt"`

Improve:

* improve the constructor so that it can implement also other types of LCGs

### xorshift

https://en.wikipedia.org/wiki/Xorshift

In particular:
* xorshift128+ , by vigna (http://vigna.di.unimi.it/xorshift/xorshift128plus.c)
* xorshift1024*, by vigna (http://vigna.di.unimi.it/xorshift/xorshift1024star.c)
* xoroshiro128+, by vigna (http://vigna.di.unimi.it/xorshift/xoroshiro128plus.c)

## TODO

* Run generators against testu01 test suite, in particular BigCrush.
* schema based on https://en.wikipedia.org/wiki/List_of_random_number_generators#Pseudorandom_number_generators_.28PRNGs.29

## ALTRO

/*
int:
7806831264735756412

bitset representation
0110 1100 0101 0111 0110 1111 1010 1100 0100 0011 1111 1101 0000 0000 0111 1100

hexdump:
7c 00 fd 43 ac 6f 57 6c

Observation: endianess reversed in memory! 
0110 1100 0101 0111 0110 1111 1010 1100 0100 0011 1111 1101 0000 0000 0111 1100
6    c    5    7    6    f    a    c    4    3    f    d    0    0    7    c

Observation: two hex per byte, bytes reversed in mem.
01101100 01010111 01101111 10101100 01000011 11111101 00000000 01111100
6   c    5   7    6   f    a   c    4   3    f   d    0   0    7   c
*/