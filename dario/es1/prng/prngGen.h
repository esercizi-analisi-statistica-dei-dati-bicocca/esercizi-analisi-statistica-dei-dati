#ifndef PRNGGEN_H
#define PRNGGEN_H

#include "prngLcg.h"

class prngGen {
    private:
    prngLcg uniform;
    double pi;

    public:
    prngGen();
    ~prngGen();
    double BreitWigner(double x, double peak = 0., double hwhm = 1.);
    double BreitWignerINV(double peak = 0., double hwhm = 1.);
    double BreitWignerTAC(double peak = 0., double hwhm = 1.,
        double xmin = -10., double xmax = 10., 
        double ymin = 0., double ymax=0.4);
    double BreitWignerRel(double x, double peak = 0., double hwhm = 1.);
    double BreitWignerRelTAC(double peak = 0., double hwhm = 1., 
        double xmin = -10., double xmax = 10., 
        double ymin = 0., double ymax=10);
};

#endif //PRNGGEN_H
