#include "point.h"
#include <iostream>

int main (){

    Point<double, 3> p0;
    p0.printPoint();

    double p1vect[] = {0., 1., 0.};
    Point<double, 3> p1(p1vect);
    p1.printPoint();
    std::cout << "0: " << p1.component(0) << ", " 
        << "1: " << p1.component(1) << ", " 
        << "2: " << p1.component(2) << std::endl ;

    Point<double, 3> p2(p1, 0,1.5);
    p2.printPoint();
    p2.scale(1.5);
    p2.printPoint();

    Point<double, 3> sum;
    sum = p1 + p2;
    sum.printPoint();    

    Point<double, 3> diff;
    diff = p1 - p2;
    diff.printPoint();

    std::cout << diff[0] << " "  << diff[1] << std::endl ;

    Point<double, 3> scaled;
    scaled = diff * 0.5 ;
    scaled.printPoint();

    std::cout << "scaled norm: " << scaled.norm() << std::endl ;

    return 0;
}