#include "range.h"
#include "emconst.h"
#include <iostream>
#include <iomanip>
#include <limits>
#include <gsl/gsl_math.h>
#include <chrono>

int main (int numArg, char* listArg[]) {

    long int start = 10 ;
    long int end   = 10000 ;
    long int dens  = 10 ;
    if (numArg >=2) { start=std::stol( listArg[1] ); }
    if (numArg >=3) { end  =std::stol( listArg[2] ); }
    if (numArg >=4) { dens =std::stol( listArg[3] ); }

    emConst eulermascheroni;
    for (auto n: logrange(start, end, dens)) {
        auto start = std::chrono::system_clock::now();
        double emresult = eulermascheroni.emConstDoubleLong(n) ;
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        std::cout << "dl," << n  << ","
            << std::setprecision(std::numeric_limits<double>::digits10+1) 
            << emresult  << ","  
            << emresult - M_EULER << "," << (emresult - M_EULER)/M_EULER << ","
            << elapsed_seconds.count()
            << std::endl;
    }
    return 0;
}