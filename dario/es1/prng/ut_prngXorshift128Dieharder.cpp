#include <iostream>
#include <string>
#include <bitset>
#include <cstdio>
#include "prngXorshift128.h"

int main(int numArg, char* listArg[]) {
    prngXorshift128 generator;
    randtype randomnumber = generator.xorshift128plus();
    std::cout.write(reinterpret_cast<char*>(&randomnumber), sizeof randomnumber) ;

    while(1) {
        randomnumber = generator.xorshift128plus(); 
        std::cout.write(reinterpret_cast<char*>(&randomnumber), sizeof randomnumber);
    }

    return 0;
}
