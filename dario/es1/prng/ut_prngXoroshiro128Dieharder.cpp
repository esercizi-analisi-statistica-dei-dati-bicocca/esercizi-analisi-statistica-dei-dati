#include <iostream>
#include <string>
#include <bitset>
#include <cstdio>
#include "prngXoroshiro128.h"

int main(int numArg, char* listArg[]) {
    prngXoroshiro128 generator;
    randtype randomnumber = generator.xoroshiro128plus();
    std::cout.write(reinterpret_cast<char*>(&randomnumber),
        sizeof randomnumber) ;

    while(1) {
        randomnumber = generator.xoroshiro128plus(); 
        std::cout.write(reinterpret_cast<char*>(&randomnumber),
            sizeof randomnumber);
    }

    return 0;
}
