/*
export LD_LIBRARY_PATH=/home/valsdav/RooUnfold:$LD_LIBRARY_PATH
g++ -o unfolding unfolding.cpp `root-config --cflags --glibs`
     -L/home/valsdav/RooUnfold -I/home/valsdav/RooUnfold/src -lRooUnfold -lUnfold
*/
#include <iostream>
#include <cmath>
#include <ctime>

#include "RooUnfold.h"
#include "RooUnfoldResponse.h"
#include "RooUnfoldBayes.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TRandom3.h"
#include "TLegend.h"
#include "TApplication.h"

using namespace std;

// Numero fenditure
#define Nfend 5
// Distanza fenditure-schermo
#define L 10
// Larghezza fenditure
#define A 0.01
// Distanza tra fenditure
#define D 1
// Lunghezza d'onda
#define lambda 450e-6
// Numero di campioni
#define Nsamp 50000
// Fattore di smearing
#define SMfactor 1

TRandom3 rnd(time(NULL));

double spettro(double x){
    double theta = atan(x / L);
    double beta = (M_PI * A / lambda) * sin(theta);
    double gamma = (M_PI * D / lambda) * sin(theta);
    return pow((sin(beta)/ beta) * (sin(Nfend*gamma)/ sin(gamma)) ,2);
}

double* smearing(double* samples, double sigma){
    double* smear = new double[Nsamp];
    for(int i = 0; i < Nsamp; i++){
        double x = samples[i];
        smear[i] = x + rnd.Gaus(x, sigma);
    }
    return smear;
}

double* sample_hit_or_miss(double xmin, double xmax, int N){
    double * samples = new double[N];
    int i = 0;
    while(i < N){
        double x = rnd.Uniform(xmin, xmax);
        double y = rnd.Rndm();
        if ( y < spettro(x)){
            samples[i] = x;
            i++;
        }
    }
    return samples;
}

int main(int argc , char* argv[]){
    TApplication* app = new TApplication("unfolding", &argc, argv);

    TCanvas* c = new TCanvas("unfolding", "unfolding", 1100, 800);

    int bins = 100;
    double xmin = -4;
    double xmax = 4;
    double sigma_smearing = SMfactor * (xmax - xmin) / bins;

    TLegend * legend = new TLegend();
    TH1D* spettro = new TH1D("spettro", "", bins, xmin, xmax);
    TH1D * spettro_measured = new TH1D("measured", "measured", bins, xmin, xmax);
    legend->AddEntry(spettro, "f_true", "l");
    legend->AddEntry(spettro_measured, "f_smeared", "l");
    spettro->GetXaxis()->SetTitle("X (cm)");
    spettro->GetYaxis()->SetTitle("Intensita'");
    spettro->SetLineWidth(2);
    spettro->SetLineColor(kBlue);
    spettro_measured->SetLineWidth(2);
    spettro_measured->SetLineColor(kRed);
   
    double * samples = sample_hit_or_miss(xmin, xmax, Nsamp);
    double * samples_smearing = smearing(samples, sigma_smearing);
    // Indipendent sample of measurements 
    double * samples_measured = smearing(sample_hit_or_miss(xmin, xmax, Nsamp), sigma_smearing);

    // Response matrix of RooUnfold
    RooUnfoldResponse response(bins, xmin, xmax);

    for (int i = 0; i < Nsamp; i++ ){
        spettro->Fill(samples[i]);
        spettro_measured->Fill(samples_measured[i]);
        //filling the responde matrix
        response.Fill(samples_smearing[i], samples[i]);
    }
    spettro->Draw();
    spettro_measured->Draw("SAME");

    // Unfolding by Bayesian iteration of RooUnfold
    RooUnfoldBayes  unfold (&response, spettro_measured, 4);
    TH1D* hist_reco = (TH1D*) unfold.Hreco();
    legend->AddEntry(hist_reco, "f_unfolded", "lp");
    hist_reco->SetLineColor(kGreen);
    hist_reco->SetLineWidth(2);
    hist_reco->SetMarkerSize(5);
    hist_reco->Draw("SAME");
    legend->Draw("SAME");

    c->Draw();

    app->Run();
}