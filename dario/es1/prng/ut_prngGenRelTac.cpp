#include <iostream>
#include <string>
#include "prngGen.h"

int main(int numArg, char* listArg[]) {
    
    int N = 10;
    if (numArg > 1){
        N = std::stoi( listArg[1] );
    }

    prngGen breit;
    for (int i=0; i<N; i++){
        std::cout 
            << breit.BreitWignerRelTAC (5.,2.,0.,20, 0.,10.) 
            << std::endl;
    }

    return 0;
}