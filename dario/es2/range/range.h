#ifndef RANGE_H
#define RANGE_H

#include <iostream>
#include <vector>
#include <stdexcept>
#include<cmath>


template <typename IntType>
std::vector<IntType> range(IntType start, IntType stop, IntType step)
{
  if (step == IntType(0)) {
    throw std::invalid_argument("step for range must be non-zero");
  }

  std::vector<IntType> result;
  IntType i = start;
  while ((step > 0) ? (i < stop) : (i > stop)) {
    result.push_back(i);
    i += step;
  }

  return result;
}

template <typename IntType>
std::vector<IntType> range(IntType start, IntType stop)
{
  return range(start, stop, IntType(1));
}

template <typename IntType>
std::vector<IntType> range(IntType stop)
{
  return range(IntType(0), stop, IntType(1));
}

template <typename IntType>
std::vector<IntType> logrange(IntType start, IntType stop, IntType ptperdec){
    IntType startpow = floor(log10(start)) ;
    IntType stoppow = floor(log10(stop)) ;
    if(stoppow<startpow){
      throw std::invalid_argument(
        "stopping value must be greater than starting value"); 
    }

    std::vector<IntType> result;
    for (IntType i = startpow; i < stoppow; i++){
        for (int j=1; j<ptperdec; j++){
            result.push_back( pow(10,i) * j/ptperdec*10 );
        }
    }

    return result;
}

#endif // RANGE_H