## Eulero-Mascheroni constant

Compute the Eulero-Mascheroni constant,
discussing the effect of errors on the result

http://mathworld.wolfram.com/Euler-MascheroniConstant.html

Use the formula: 
```.latex
lim_{n->\infty} \left( \sum_{k=1}^n \frac{1}{k} - log(n) \right)
```

```
0.577215664901532860606512090082402431042159335939923598805
```

### compile and launch

`./compile.sh`

`python3 launch.py > data/data.csv`

### requirements

Whole project
* gsl libraries: in order to retrieve `M_EULER`

Arbitrary precision directory
* GMP: emGmp.cpp
* boost: emBoost.cpp emGmp1.cpp emMpfr.cpp
* mpfr: emMpfr.cpp

