\documentclass[relazione.tex]{subfiles}

\begin{document}

\chapter{Settimo Esercizio}

In questo esercizio si analizzano alcune tecniche di Multi Variate Analysis implementate in TMVA per eseguire un'operazione di classificazione che consiste in distinzione tra segnale e fondo.

\section{Generazione del segnale e del fondo}

Come distribuzioni di segnale e fondo vengono scelte due gaussiane bivariate, la prima centrata in $[4,4]$ e con $\sigma_x = \sigma_y = 1$ e $\rho = 0$, mentre la seconda centrata in $[0,1]$ con $\sigma_x = \sigma_y = 1.5$ e $\rho = 0.7$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../es7/gauss1-tex}
	\caption{segnale in rosso e fondo in blu}
	\label{fig:gauss1-tex}
\end{figure}

Il segnale e il fondo sono salvati sullo stesso ROOT file dentro a due TTree diversi.
\begin{minted}{c}
TFile* outfile = new TFile("sigbg.root", "RECREATE");
TTree* sigTree = new TTree("Signal", "Gaussian Signal");
sigTree->Branch("x", &sigX);
sigTree->Branch("y", &sigY);
TTree* bgTree = new TTree("Background", "Gaussian Background");
bgTree->Branch("x", &bgX);
bgTree->Branch("y", &bgY);
\end{minted}

\section{Implementazione della classificazione con TMVA}

TMVA si occupa di approcciare due tipologie diverse di problemi. La prima è la classificazione mentre la seconda è la regressione. Per ciascuna di queste tipologie TMVA può essere usato prima per testare le performance di un dato algoritmo nell'approcciare un dato problema, fase di \emph{training}, e in secondo luogo per usare usare quell'algoritmo per risolvere un problema reale, fase di \emph{application}.

In questo esercizio si esegue solo la fase di test su alcuni algoritmi di MVA.

Si comincia con aprire il ROOT file appena generato e importare i due TTree.

\begin{minted}{c}
TFile* infile = new TFile("sigbg.root", "READ");
TTree* sigTree = (TTree*) infile->Get("Signal");
TTree* bgTree = (TTree*) infile->Get("Background");
\end{minted}

Si prosegue con la creazione di un oggetto TMVA::Factory che permette di interfacciare i dati con le classi TMVA. Si genera un oggeto TMVA::Dataloader che si occupa di importare i dati dai TTree.

\begin{minted}{c}
TMVA::Factory* factory = new TMVA::Factory("<name>", outfile, "<options>" ) ;

TMVA::DataLoader* dataloader = new TMVA::DataLoader("dataset") ;
dataloader->AddVariable( "x", "Variable 1",'F');    
dataloader->AddVariable( "y", "Variable 2",'F');
double sigWeight = 1.0, bgWeight = 1.0 ;
dataloader->AddSignalTree(sigTree, sigWeight);
dataloader->AddBackgroundTree (bgTree, bgWeight) ;
TCut sigcut = "", bgcut = "" ;
dataloader->PrepareTrainingAndTestTree(sigcut, bgcut, "SplitMode=Random") ;
\end{minted}

Si scelgono gli algoritmi e i relativi parametri con cui compiere l'analisi, ad esempio
\begin{minted}{c}
factory->BookMethod(dataloader, TMVA::Types::kFisher,"Fisher","<options>");
\end{minted}

Si conclude con il training, l'analisi e la valutazione dei metodi specificati:
\begin{minted}{c}
factory->TrainAllMethods();
factory->TestAllMethods();
factory->EvaluateAllMethods();
\end{minted}

\section{Analisi dei risultati}

Si confrontano le performance dei seguenti metodi:
\begin{itemize}
	\item Fisher
	\item Likelihood
	\item Multi Layer Perceptron (600 cicli di training)
	\item BDT
\end{itemize}

È possibile analizzare l'output di TMVA e ottenere informazioni sui pofili delle vvaribili di input, le separazioni tra fondo e segnale, le efficienze separazione tra fondo e segnale in base al valore del taglio

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/variables_id_c1}
	\caption{Profili delle distribuzioni degli eventi nelle variabili x e y}
	\label{fig:variablesidc1}
\end{figure}

\begin{figure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/mva_Fisher}
\caption{Fisher}
\label{fig:mvafisher}	
\end{subfigure}	
\begin{subfigure}{.5\textwidth}
	\centering
	\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/mva_BDT}
	\caption{BDT}
	\label{fig:mvabdt}
\end{subfigure}	
\begin{subfigure}{.5\textwidth}
	\centering
	\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/mva_Likelihood}
	\caption{Likelihood}
	\label{fig:mvalikelihood}	
\end{subfigure}	
\begin{subfigure}{.5\textwidth}
	\centering
\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/mva_MLP}
\caption{MLP}
\label{fig:mvamlp}
\end{subfigure}		
\end{figure}

\begin{figure}
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/mvaeffs_Fisher}
		\caption{Fisher}
		\label{fig:mvafishereffs}	
	\end{subfigure}	
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/mvaeffs_BDT}
		\caption{BDT}
		\label{fig:mvabdteffs}
	\end{subfigure}	
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/mvaeffs_Likelihood}
		\caption{Likelihood}
		\label{fig:mvalikelihoodeffs}	
	\end{subfigure}	
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/mvaeffs_MLP}
		\caption{MLP}
		\label{fig:mvamlpeffs}
	\end{subfigure}		
\end{figure}

Si valuta infine la performance di ciascun algoritmo guardando ai valori degli integrali delle ROC.

\begin{minted}{latex}
: Evaluation results ranked by best signal efficiency and purity (area)
: ---------------------------------------------------------------------
: DataSet       MVA
: Name:         Method:          ROC-integ
: dataset       MLP            : 0.991
: dataset       Fisher         : 0.990
: dataset       BDT            : 0.990
: dataset       Likelihood     : 0.989
\end{minted}

Il Multi Layer Perceptron risulta essere l'algoritmo che meglio riesce a separare in questo esempio il segnale dal rumore, seppur con un basso margine rispetto agli altri metodi.

È inoltre possibile anche avere informazioni sullo stato interno di ciascun algoritmo, ad esempio è possibile ottenere la struttura interna della rete di MLP e della velocità con cui è arrivato alla convergenza dei parametri.

\begin{figure}
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/annconvergencetest}
\caption{Convergenza della rete}
\label{fig:annconverrgencetest}	
\end{subfigure}	
\begin{subfigure}{.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth]{../es7/dataset/plots/network}
\caption{Struttura interna della rete}
\label{fig:network}
\end{subfigure}		
\end{figure}

\end{document}
