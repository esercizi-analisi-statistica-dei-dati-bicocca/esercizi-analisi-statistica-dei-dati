#include <iostream>
#include <string>
#include "prngGen.h"

int main(int numArg, char* listArg[]) {
    
    int N = 10;
    if (numArg > 1){
        N = std::stoi( listArg[1] );
    }

    prngGen breit;
    for (int i=0; i<N; i++){
        std::cout << breit.BreitWignerINV () << std::endl;
    }

    return 0;
}