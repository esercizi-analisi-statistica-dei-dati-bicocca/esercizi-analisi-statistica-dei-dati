#!/bin/bash

rm -rf build
rm -rf install
mkdir build
mkdir install
mkdir data

CURRENT_PATH=$PWD

cd build
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="$CURRENT_PATH/install" ..
make
make install
cd ..

# rsync -au --stats $CURRENT_PATH/ compute:/root/stat/es3