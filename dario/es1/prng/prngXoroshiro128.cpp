#include "prngXoroshiro128.h"
#include <climits>
#include <cmath>

prngXoroshiro128::prngXoroshiro128(){
    a = 55 ;
    b = 14 ;
    c = 36 ;
    s[0] = uniform.randInt() ;
    s[1] = uniform.randInt() ;
}

prngXoroshiro128::~prngXoroshiro128(){}

randtype prngXoroshiro128::rotl(const randtype x, int k ){
    return (x << k) | ( x >> ( 64 - k )) ;
}

randtype prngXoroshiro128::xoroshiro128plus(){
    const randtype s0 = s[0] ;
    randtype s1 = s[1] ;
    const randtype result = s0 + s1;

    s1 ^= s0 ;
    s[0] = rotl(s0, a) ^ s1 ^ (s1 << b) ;
    s[1] = rotl(s1, c) ;

    return result ;
}

double prngXoroshiro128::rand(double min, double max){
    return (double) (max - min) * ( (double) xoroshiro128plus() ) 
        / ( (double) ULLONG_MAX ) + min ;
}

double prngXoroshiro128::gauss(double mu, double sigma){
    // box-muller algorithm
    double u1 = rand(0,1);
    double u2 = rand(0,1);
    double z0 = sqrt( -2.0 * log(u1) ) * cos (2*M_PI*u2);
    // double z1: discarded
    return z0 * sigma + mu ;
}