#!/bin/bash

g++ -o signalbackground signalbackground.cpp -g `root-config --glibs --cflags`
g++ -o tmva tmva.cpp `root-config --glibs --cflags` -lTMVA