

## compile

`./compile.sh`

old:
g++ -o unfolding unfolding.cpp `root-config --cflags --glibs` -L/home/dario/software/roounfold/RooUnfold -I/home/dario/software/roounfold/RooUnfold/src -lRooUnfold -lUnfold

## run

`export LD_LIBRARY_PATH=/home/dario/software/roounfold/RooUnfold:$LD_LIBRARY_PATH`
`./install/bin/unfold`

## prereq

add `#include "TBuffer.h"` in `src/RooUnfold.cxx` and `src/RooUnfoldSvd.cxx`
then it crashed because it laks `rlibmap`, but it is used for packaging 
purposes, it is not necessary
