/*
g++ -o tmva tmva.cpp `root-config --glibs --cflags` -lTMVA

The TMVA training job runs alternatively 
* as a ROOT script, 
* as a standalone executable, where libTMVA.so is linked as a shared library, 
* or as a python script via the PyROOT interface.

this triggered the command to compile.
*/
#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TCut.h"

#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"

int main () {


    TFile* infile = new TFile("sigbg.root", "READ");
    TTree* sigTree = (TTree*) infile->Get("Signal");
    TTree* bgTree = (TTree*) infile->Get("Background");

    TFile* outfile = new TFile("tmva-status.root", "RECREATE");
    TMVA::Factory* factory = new TMVA::Factory("TMVAClassification", outfile, 
        "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" ) ;

    TMVA::DataLoader* dataloader = new TMVA::DataLoader("dataset") ;
    dataloader->AddVariable( "x", "Variable 1",'F');    
    dataloader->AddVariable( "y", "Variable 2",'F');

    double sigWeight = 1.0 ;
    double bgWeight = 1.0 ;

    dataloader->AddSignalTree(sigTree, sigWeight);
    dataloader->AddBackgroundTree (bgTree, bgWeight) ;

    TCut sigcut = "" ;
    TCut bgcut = "" ;

    dataloader->PrepareTrainingAndTestTree(sigcut, bgcut, "SplitMode=Random") ;
    // "NSigTrain=3000:NBkgTrain=3000:NSigTest=3000:NBkgTest=3000:
    // SplitMode=Random:!V"
    // "nTrain_Signal=1000:nTrain_Background=1000:SplitMode=Random:
    // NormMode=NumEvents:!V"

    factory->BookMethod( dataloader, TMVA::Types::kFisher, "Fisher", 
        "H:!V:Fisher:VarTransform=None" );
    factory->BookMethod( dataloader, TMVA::Types::kMLP, "MLP", 
        "H:!V:VarTransform=N:NCycles=600:HiddenLayers=N+5:TestRate=5:!UseRegulator" );
    factory->BookMethod( dataloader, TMVA::Types::kLikelihood, "Likelihood",
        "H:!V" );
    factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT", 
        "!H:!V" );

    factory->TrainAllMethods();
    factory->TestAllMethods();
    factory->EvaluateAllMethods();

    outfile->Close();

    std::cout << "==> Wrote root file: " << outfile->GetName() << std::endl;
    std::cout << "==> TMVAClassification is done!" << std::endl;

    delete factory ;
    delete dataloader ;

    return 0;
}