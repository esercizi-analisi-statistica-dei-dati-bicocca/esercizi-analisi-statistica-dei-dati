/*
g++ -o tmva tmva.cpp `root-config --glibs --cflags` -lTMVA

The TMVA training job runs alternatively 
* as a ROOT script, 
* as a standalone executable, where libTMVA.so is linked as a shared library, 
* or as a python script via the PyROOT interface.

this triggered the command to compile.
*/
#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TCut.h"

#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"

int main () {


    TFile* infile = new TFile("signal.root", "READ");
    TTree* sigTree = (TTree*) infile->Get("mw_mjj");
    TFile* infile2 = new TFile("bkg.root", "READ");
    TTree* bgTree = (TTree*) infile2->Get("mw_mjj");

    TFile* outfile = new TFile("tmva-status.root", "RECREATE");
    TMVA::Factory* factory = new TMVA::Factory("TMVAClassification", outfile, 
        "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" ) ;

    TMVA::DataLoader* dataloader = new TMVA::DataLoader("dataset") ;
    // dataloader->AddVariable( "mu_eta", "Variable 1",'F');    
    dataloader->AddVariable( "mjj_vbs", "Variable 2",'F');
    dataloader->AddVariable( "deltaeta_vjet", "Variable 2",'F');
    dataloader->AddVariable( "deltaeta_vbs", "Variable 2",'F');
    dataloader->AddVariable( "vjet_pt", "Variable 2",'F');
    // dataloader->AddVariable( "Zmu", "Variable 2",'F');
    // dataloader->AddVariable( "C_ww", "Variable 2",'F');


    double sigWeight = 1.0 ;
    double bgWeight = 1.0 ;

    dataloader->AddSignalTree(sigTree, sigWeight);
    dataloader->AddBackgroundTree (bgTree, bgWeight) ;

    TCut sigcut = "( abs(mu_eta) < 2.1) && (deltaeta_vbs > 4) &&  (mjj_vbs > 350) && (mjj_vjet > 65) && (mjj_vjet < 105)  && (Mww > 400) " ;
    TCut bgcut = "( abs(mu_eta) < 2.1) && (deltaeta_vbs > 4) && (mjj_vbs > 350) && (mjj_vjet > 65) && (mjj_vjet < 105)  &&(Mww > 400) " ;

    dataloader->PrepareTrainingAndTestTree(sigcut, bgcut, "SplitMode=Random") ;
    // "NSigTrain=3000:NBkgTrain=3000:NSigTest=3000:NBkgTest=3000:
    // SplitMode=Random:!V"
    // "nTrain_Signal=1000:nTrain_Background=1000:SplitMode=Random:
    // NormMode=NumEvents:!V"

    factory->BookMethod( dataloader, TMVA::Types::kFisher, "Fisher", 
        "H:!V:Fisher:VarTransform=None" );
    factory->BookMethod( dataloader, TMVA::Types::kMLP, "MLP", 
        "H:!V:VarTransform=N:NCycles=600:HiddenLayers=N+5:TestRate=5:!UseRegulator" );
    factory->BookMethod( dataloader, TMVA::Types::kLikelihood, "Likelihood",
        "H:!V" );
    factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT", 
        "!H:!V" );

    factory->TrainAllMethods();
    factory->TestAllMethods();
    factory->EvaluateAllMethods();

    outfile->Close();

    std::cout << "==> Wrote root file: " << outfile->GetName() << std::endl;
    std::cout << "==> TMVAClassification is done!" << std::endl;

    delete factory ;
    delete dataloader ;

    return 0;
}