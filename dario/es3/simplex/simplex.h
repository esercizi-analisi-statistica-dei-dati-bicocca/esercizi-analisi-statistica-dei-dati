#ifndef SIMPLEX_H
#define SIMPLEX_H

#include "point.h"
#include <vector>
#include <functional>
#include <gsl/gsl_math.h>
#include <cmath>

template <typename myfloat, int N>
class Simplex{
    public:
    Simplex(std::function<myfloat (Point<myfloat, N>)> f);
    ~Simplex(){
        simp.clear();
        y.clear();
    };
    std::function <myfloat (Point<myfloat, N>)> func ;
    Point<myfloat, N> minimize(Point<myfloat, N> p0, 
        myfloat delta, myfloat newftol, myfloat newptol , int newmaxsteps);
    void printResult();

    private:
    myfloat ftolthreshold; // tolerance
    myfloat ptolthreshold; // tolerance
    myfloat ftol;
    myfloat ptol;
    myfloat tiny; 
    int nsteps; // number of iterations
    int maxsteps;
    myfloat delta;
    std::vector<Point<myfloat, N>> simp; // current simplex, made by N+1 points
    std::vector<myfloat> y; // function values at the vertices of the simplex
    Point<myfloat, N> pointSum();
    Point<myfloat, N> getCentroid();
    Point<myfloat, N> oldCentroid;
    myfloat pointTry(myfloat scale);
    myfloat getFtol();
    myfloat getPtol();
    void contractLowest();
    void printState();
    int indexHigh = 0 ;
    int indexNextHigh = 0 ;
    int indexLow = 0 ;
    double alpha = 1 ; // reflection
    double gamma = 2 ; // expansion
    double rho = 0.5 ; // contraction 
    double sigma = 0.5; // shrink
};

template <typename myfloat, int N>
Simplex<myfloat, N>::Simplex(std::function<myfloat (Point<myfloat, N>)> f):
    indexHigh(0),
    indexNextHigh(0),
    indexLow(0),
    delta(0.1),
    ftol(1.),
    ptol(1.)
{
    nsteps = 0 ;
    maxsteps = 3000;
    func = f;
}

template <typename myfloat, int N>
Point<myfloat, N> Simplex<myfloat, N>::minimize
        (Point<myfloat, N> p0, 
        myfloat newdelta, myfloat newftol, myfloat newptol, 
        int newmaxsteps) {
    ftolthreshold = newftol;
    ptolthreshold = newptol;
    maxsteps = newmaxsteps ;
    delta = newdelta ;
    // p0: initial point
    // we start by constructing the initial simplex
    simp.clear();
    simp.push_back(p0);
    y.push_back(func(p0)) ;
    for (int i=0; i<N; i++) {
        Point<myfloat, N> vi(p0, i, delta);
        simp.push_back(vi);
        y.push_back(func(vi));
    }

    nsteps = 0;
    while(1) {
        // get high, next high and lowest points
        if (y[0]>y[1]) {
            indexNextHigh = 1;
            indexHigh = 0;
        } else {
            indexNextHigh = 0;
            indexHigh = 1;      
        }
        for (int i=0; i<y.size(); i++){
            if (y[i] < y[indexLow]) indexLow=i;
            if (y[i] >= y[indexHigh]){
                indexNextHigh = indexHigh;
                indexHigh = i;
            } else if (y[i] > y[indexNextHigh] && i!= indexHigh){
                indexNextHigh = i ;
            }
        }

        // check exit conditions
        ftol = getFtol();
        ptol = getPtol();
        if (ftol < ftolthreshold || ptol < ptolthreshold) {
            return simp[indexLow];
        }
        if (nsteps > maxsteps) {
            throw("nsteps exceeded"); 
        }
        nsteps +=2;

        // new iteration
        // reflect from the high point
        oldCentroid = getCentroid();
        myfloat ytry = pointTry(- alpha );
        if (ytry <= y[indexLow]) {
            // better result than the low point, try addition extrapolation
            ytry = pointTry(gamma);
        } else if ( ytry >= y[indexNextHigh] ) {
            // reflected is worse than second high, try intermidiate
            // contraction by 1D
            myfloat ysave = y[indexHigh] ;
            ytry = pointTry(rho);
            if ( ytry >= ysave ) {
                // cant get rid of the high point
                // contract around the lowest
                contractLowest();
            }
            nsteps += N ;
        } else {
            nsteps-- ; 
        }
    }

}

template <typename myfloat, int N>
Point<myfloat, N> Simplex<myfloat, N>::pointSum(){
    Point<myfloat, N> sum;
    for (int j=0; j<N; j++){
        sum[j] = 0;
        for (int i=0; i<N+1; i++){
            sum[j] = sum[j] + simp[i][j] ;
        }
    }
    return sum;
}

template <typename myfloat, int N>
myfloat Simplex<myfloat, N>::pointTry(myfloat scale){
    Point<myfloat, N> sum;
    sum = pointSum();
    myfloat fac1 = (1.0-scale) / ( (double) N) ;
    myfloat fac2 =  fac1 - scale;
    Point<myfloat, N> ptry;
    ptry = sum*fac1 - simp[indexHigh]*fac2;
    myfloat ytry = func(ptry);
    if ( ytry < y[indexHigh] ){ 
        y[indexHigh] = ytry;
        simp[indexHigh] = ptry ;
    }
    return ytry;
}

template <typename myfloat, int N>
void Simplex<myfloat, N>::contractLowest(){
    for (int i=0; i<N+1; i++){
        if ( i != indexLow) {
            Point<myfloat, N> temp;
            simp[i] = (simp[i]+simp[indexLow]) * sigma ;
            y[i] = func(simp[i]) ;
        }
    }
}

template <typename myfloat, int N>
myfloat Simplex<myfloat, N>::getFtol(){
    myfloat yold = func(oldCentroid) ;
    myfloat ynew = func(getCentroid()) ;
    myfloat num = abs( yold - ynew )  ;
    myfloat denom = abs(yold)  ;
    return num / denom;
}

template <typename myfloat, int N>
myfloat Simplex<myfloat, N>::getPtol(){
    Point<myfloat,N> centroid (getCentroid());
    myfloat num = (centroid-oldCentroid).norm() ;
    myfloat denom = oldCentroid.norm() ;
    return   num / denom  ;
}

template <typename myfloat, int N>
Point<myfloat, N> Simplex<myfloat, N>::getCentroid(){
    myfloat centervec [N];
    for (int j=0; j<N; j++){
        centervec[j]=0;   
        for (auto it = simp.begin(); it != simp.end(); it++) {
            centervec[j] += (*it)[j] ;
        }
        centervec[j] /= N+1 ; 
    }
    Point<myfloat, N> centroid(centervec);
    return centroid;
}

template <typename myfloat, int N>
void Simplex<myfloat, N>::printState(){
    std::cout << " nsteps: " << nsteps << std::endl;
    std::cout << " state: " << std::endl;
    for (auto it = simp.begin(); it != simp.end(); it++) {
        it->printPoint() ;
    }
    std::cout << " ";
    for (auto it = y.begin(); it != y.end(); it++) {
        std::cout << *it << " " ;
    }
    std::cout << std::endl ;
    std::cout << " ftol: " << getFtol() << ", ptol: " << getPtol() << std::endl ;
    std::cout << " oldcentroid:" << std::endl;
    oldCentroid.printPoint();
    std::cout << " centroid:" << std::endl;
    getCentroid().printPoint();
}

template <typename myfloat, int N>
void Simplex<myfloat, N>::printResult(){
    // (xstart, ystart,) (xmin, ymin, f@min,) iters, ftol, ptols
    std::cout << nsteps << "," << ftol << "," << ptol << std::endl ;
}

#endif // SIMPLEX_H