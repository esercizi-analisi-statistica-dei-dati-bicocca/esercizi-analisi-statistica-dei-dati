#include "range.h"
#include "emconst.h"
#include <iostream>
#include <iomanip>
#include <limits>
#include <gsl/gsl_math.h>
#include <chrono>

int main (int numArg, char* listArg[]) {

    int start = 10 ;
    int end   = 10000 ;
    int dens  = 10 ;
    if (numArg >=2) { start=std::stoi( listArg[1] ); }
    if (numArg >=3) { end  =std::stoi( listArg[2] ); }
    if (numArg >=4) { dens =std::stoi( listArg[3] ); }

    emConst eulermascheroni;
    for (auto n: logrange(start, end, dens)) {
        auto start = std::chrono::system_clock::now();
        double emresult = eulermascheroni.emConstDoubleInt(n) ;
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        std::cout << "di," << n  << ","
            << std::setprecision(std::numeric_limits<double>::digits10+1) 
            << emresult  << ","  
            << emresult - M_EULER << "," << (emresult - M_EULER)/M_EULER << ","
            << elapsed_seconds.count()
            << std::endl;
    }

    return 0;
}