#include "emconst.h"
#include "sum.h"
#include <cmath>

emConst::emConst(){}
emConst::~emConst(){}

float emConst::emConstFloatInt(int n){
    Sum<float, int> sum ;
    return sum.directSum(n) - log( (float) n) ;
}

float emConst::emConstFloatIntKahan(int n){
    Sum<float, int> sum ;
    return sum.KahanSum(n) - log( (float) n) ;
}

double emConst::emConstDoubleInt(int n){
    Sum<double, int> sum ;
    return sum.directSum(n) - log( (double) n) ;
}

double emConst::emConstDoubleIntKahan(int n){
    Sum<double, int> sum ;
    return sum.KahanSum(n) - log( (double) n) ;
}

double emConst::emConstDoubleLong(long int n){
    Sum<double, long int> sum ;
    return sum.directSum(n) - log( (double) n) ;
}

double emConst::emConstDoubleLongKahan(long int n){
    Sum<double, long int> sum ;
    return sum.KahanSum(n) - log( (double) n) ;
}

long double emConst::emConstLongdoubleLong(long int n){
    Sum<long double, long int> sum ;
    return sum.directSum(n) - logl( (long double) n) ;
}