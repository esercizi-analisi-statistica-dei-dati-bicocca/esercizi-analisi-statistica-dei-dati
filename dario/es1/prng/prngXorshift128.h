#ifndef prngXorshift128128_H
#define prngXorshift128128_H

#include "prngLcg.h"

typedef unsigned long long int randtype;

class prngXorshift128 {
    private:
    prngLcg uniform ;
    int a ;
    int b ;
    int c ;
    randtype s[2] ;

    public:
    prngXorshift128();
    ~prngXorshift128();
    randtype xorshift128plus();
    double rand(double min=0., double max=1.); 
};

#endif //prngXorshift128128_H