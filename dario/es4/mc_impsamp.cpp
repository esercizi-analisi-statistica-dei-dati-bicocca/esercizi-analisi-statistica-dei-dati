#include "mc.h"
#include "prngXoroshiro128.h"
#include "range.h"
#include <iostream>
#include <cmath>
#include <chrono>

int main (int numArg, char* listArg[]) {

    int start = 10 ;
    int end = 10000000 ;
    int dens = 10 ; // points per decade
    if (numArg >=2) { start=std::stoi( listArg[1] ); }
    if (numArg >=3) { end  =std::stoi( listArg[2] ); }
    if (numArg >=4) { dens =std::stoi( listArg[3] ); }

    mc<prngXoroshiro128> montecarlo(10);
    for (auto i: logrange(start, end, dens)) {
        montecarlo.set_parameters(i);
        auto start = std::chrono::system_clock::now();
        montecarlo.f_mc_impsamp();
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        std::cout << "imp," <<  i << ","
            << montecarlo.f_get_integral() << ","
            << montecarlo.f_get_stdev() << ","
            << elapsed_seconds.count()
            << std::endl;
    }

    return 0;
}