#ifndef SUM_H
#define SUM_H

template < typename myfloat, typename myint >
class Sum {
    public:
    Sum () {
        sum = 0 ;
        c = 0. ;
    };
    ~Sum (){};

    myfloat directSum(myint n = 1000){
        sum = 0;
        for (myint k=1; k <= n; k++){
            sum += 1. / (myfloat) k ;
        }
        return sum;
    } ;
    
    myfloat KahanSum(myint n = 1000){
        sum = 0;
        c = 0.;
        for (myint k=1; k <=n; k++){
            myfloat y = 1. / ( (myfloat) k) - c ;
            myfloat t = sum + y ;
            c = (t - sum) - y ;
            sum = t ;
        }
        return sum;
    } ;

    private:
    myfloat sum ;
    myfloat c ;
};


#endif