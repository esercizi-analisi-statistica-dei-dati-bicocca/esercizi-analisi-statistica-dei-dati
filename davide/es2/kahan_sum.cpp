#include <iostream>
#include <cmath>
#include <numeric>
#include <limits>
#include <iomanip>
#include <fstream>
#include <gsl/gsl_math.h>
#include "TGraph.h"
#include "TCanvas.h"
#include "TMultiGraph.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLegend.h"

using namespace std;

double euler_double(int n){
    double sum = 0.;
    for (int i = 1; i<=n; i++){
        sum += 1 / (double)i;
    } 
    sum -= log(n);
    return sum;
}

float euler_float(int n){
    float sum = 0.;
    for (int i = 1; i<=n; i++){
        sum += 1 / (double)i;
    }
    sum -= log(n);
    return sum;
}

float euler_float_kahan(int n){
    float sum = 0.;
    float c = 0.;
    for (int i = 1; i<=n; i++){
        float y = (1/(double)i) - c ;
        float t =  sum + y ;
        c = (t - sum)  - y;
        sum = t;
    }
    sum -= log(n);    
    return sum;
}

float pairwise_sum(int nstart, int nstop){
    if ((nstop - nstart ) < 100){
        float sum = 0.;
        for (int i = nstart; i<=nstop; i++){
            sum += 1 / (double)i;
        }
        return sum;
    }else{
        int m = floor((nstop - nstart) /2 );
        return pairwise_sum(nstart, nstart+m) + pairwise_sum(nstart+m+1, nstop);
    }
}

float euler_pairwise(int n){
    float psum = pairwise_sum(1, n);
    return psum - log(n);
}


int main(){
    cout << "Euler constant: " << setprecision(numeric_limits<long double>::digits10 +1) << M_EULER << endl;
    
    double * results_double = new double[16];
    double * results_float = new double[16];
    double * results_float_kahan = new double[16];
    double * results_float_pairwise = new double[16];
    double * xs = new double[16];
 
    for (int i = 2; i <= 18; i++){
        double x = pow(10, (double)i/2);
        xs[i-2] = x;
        double e_double  = euler_double(x);
        double e_float  = euler_float(x);
        double e_float_kahan  = euler_float_kahan(x);
        double e_float_pairwise  = euler_pairwise(x);

        cout << "Double         " << i << "  " << setprecision(numeric_limits<double>::digits10 +1) << e_double-M_EULER <<endl;
        cout << "Float          " << i << "  " << setprecision(numeric_limits<float>::digits10 +1) << e_float-M_EULER <<endl;
        cout << "Float Kahan    " << i << "  " << setprecision(numeric_limits<float>::digits10 +1) << e_float_kahan-M_EULER <<endl;
        cout << "Float Pairwise " << i << "  " << setprecision(numeric_limits<float>::digits10 +1) << e_float_pairwise-M_EULER <<endl;

        results_double[i-2] = abs(e_double - M_EULER);
        results_float[i-2] = abs(e_float - M_EULER);
        results_float_kahan[i-2] = abs(e_float_kahan - M_EULER);
        results_float_pairwise[i-2] = abs(e_float_pairwise - M_EULER);
    }

    gStyle->SetPalette(kBlueGreenYellow);
    TCanvas * c = new TCanvas("canvas", "Euler constant estimation", 800, 600);
    TLegend * leg  = new TLegend();

    TGraph * g_double = new TGraph(16, xs, results_double);    
    TGraph * g_float = new TGraph(16, xs, results_float);
    TGraph * g_float_kahan = new TGraph(16, xs, results_float_kahan);
    TGraph * g_float_pairwise = new TGraph(16, xs, results_float_pairwise);

    leg->AddEntry(g_double, "double", "lp");
    leg->AddEntry(g_float, "float", "lp");
    leg->AddEntry(g_float_kahan, "float_kahan", "lp");
    leg->AddEntry(g_float_pairwise, "float_pairwise", "lp");

    TMultiGraph * mg = new TMultiGraph();
    mg->SetTitle("Euler constant estimation;N iterations;#gamma euler - #gamma");
    mg->Add(g_double);
    mg->Add(g_float);
    mg->Add(g_float_kahan);
    mg->Add(g_float_pairwise);
    mg->Draw("AC* PMC PLC");
    leg->Draw("same");

    c->SetLogx();
    c->SetLogy();
    c->SaveAs("euler_constant.pdf");
}