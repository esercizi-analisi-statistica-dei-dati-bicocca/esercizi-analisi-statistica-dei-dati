#include "point.h"
#include "simplex.h"
#include <iostream>

double f1 (Point<double, 2> p){
    return p[0]*p[0]+p[1]*p[1] ;
}

int main(int numArg, char* listarg[]) {

    double input[] = {4., 4.} ;
    Point<double, 2> p0(input) ;

    Simplex<double, 2> simplesso (f1);
    simplesso.minimize(p0, 1., 1e-10, 1e-10, 3000).printPoint();

    return 0;
}