#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <vector>
#include <cmath>

template <typename myfloat, int N>
class Point {
    public:
    Point();
    Point(myfloat* p0);
    Point( Point<myfloat, N> p0, int component, myfloat value );
    Point(const Point& p);
    ~Point(){};
    void scale(myfloat scalefactor);
    void printPoint();
    myfloat norm();
    myfloat component(int i);
    Point& operator= (const Point& p);
    Point operator+ (const Point& p);
    Point operator- (const Point& p);
    Point operator* (myfloat scale);
    myfloat& operator[] (int component);

    private:
    myfloat point[N];
};

template <typename myfloat, int N>
Point<myfloat, N>::Point(){
    for (int i=0; i<N; i++){
        point[i] = 0. ;
    }
}

template <typename myfloat, int N>
Point<myfloat, N>::Point(myfloat* p0){
    // this may cause segfaults
    // not bound checked
    for (int i=0; i<N; i++){
        point[i] = p0[i] ;
    }
}

template <typename myfloat, int N>
Point<myfloat, N>::Point( Point<myfloat, N> p0, int component, myfloat value){
    if (component >= N) {
        // TODO
        // FIXME
        throw("Point constructor: component higher than N");
    }
    for (int i=0; i<N; i++){
        double xi = p0[i] ;
        if (i == component){
            xi += value;
        }
        point[i] = xi;
    }
}

template <typename myfloat, int N>
Point<myfloat, N>::Point(const Point& p){
    for (int i=0; i<N; i++){
        point[i] = p.point[i] ;
    }
}

template <typename myfloat, int N>
void Point<myfloat, N>::scale(myfloat scalefactor){
    for (int i=0; i<N; i++ ){
        point[i] = point[i] * scalefactor ;
    }
};

template <typename myfloat, int N>
void Point<myfloat, N>::printPoint(){
    std::cout << " ( " ;
    for (int i=0; i<N; i++ ){
        std::cout << point[i] << " " ;
    }
    std::cout << ")" << std::endl;
};

template <typename myfloat, int N>
Point<myfloat, N>& Point<myfloat, N>::operator=(const Point& p){
    for (int i=0; i<N; i++ ){
        point[i] = p.point[i];
    }
    return *this;
};

template <typename myfloat, int N>
Point<myfloat, N> Point<myfloat, N>::operator+(const Point& p){
    Point<myfloat, N> sum;
    for (int i=0; i<N; i++ ){
        sum[i] = point[i] + p.point[i] ;
    }
    return sum;
};

template <typename myfloat, int N>
Point<myfloat, N> Point<myfloat, N>::operator-(const Point& p){
    Point<myfloat, N> diff;
    for (int i=0; i<N; i++ ){
        diff[i] = point[i] - p.point[i] ;
    }
    return diff;
};

template <typename myfloat, int N>
Point<myfloat, N> Point<myfloat, N>::operator*(myfloat scale){
    Point<myfloat, N> scaledpoint;
    for (int i=0; i<N; i++ ){
        scaledpoint[i] = point[i] * scale ;
    }
    return scaledpoint;
};

template <typename myfloat, int N>
myfloat& Point<myfloat, N>::operator[](int component){
    return point[component];
};

template <typename myfloat, int N>
myfloat Point<myfloat, N>::norm(){
    myfloat norm = 0;
    for (int i=0; i<N; i++){
        norm += point[i]*point[i] ;
    }
    return sqrt(norm);
};

template <typename myfloat, int N>
myfloat Point<myfloat, N>::component(int i){
    return point[i];
};

#endif //POINT_H