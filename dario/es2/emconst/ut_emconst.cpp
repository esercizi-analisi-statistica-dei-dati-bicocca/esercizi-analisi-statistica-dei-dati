#include "emconst.h"
#include <iostream>
#include <iomanip>
#include <limits>

int main () {

    emConst eulermascheroni;
    std::cout << std::setprecision(std::numeric_limits<float>::digits10+1) 
        << eulermascheroni.emConstFloatInt(10000) << std::endl;
    std::cout << std::setprecision(std::numeric_limits<double>::digits10+1) 
        << eulermascheroni.emConstDoubleInt(10000) << std::endl;
    std::cout << std::setprecision(std::numeric_limits<double>::digits10+1) 
        << eulermascheroni.emConstDoubleLong(10000) << std::endl;
    std::cout << std::setprecision(std::numeric_limits<long double>::digits10+1) 
        << eulermascheroni.emConstLongdoubleLong(10000) << std::endl;

    return 0;
}