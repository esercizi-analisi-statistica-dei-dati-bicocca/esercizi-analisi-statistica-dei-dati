#ifndef prngXoroshiro128_H
#define prngXoroshiro128_H

#include "prngLcg.h"

typedef unsigned long long int randtype;

class prngXoroshiro128 {
    private:
    prngLcg uniform ;
    int a ;
    int b ;
    int c ;
    randtype s[2] ;

    public:
    prngXoroshiro128();
    ~prngXoroshiro128();
    randtype rotl(const randtype x, int k );
    randtype xoroshiro128plus();
    double rand(double min=0., double max=1.); 
    double gauss(double mu=0., double sigma=1.); 
};

#endif //prngXoroshiro128_H