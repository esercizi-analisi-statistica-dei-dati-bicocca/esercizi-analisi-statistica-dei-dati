#include "prngXorshift1024.h"
#include <climits>

prngXorshift1024::prngXorshift1024(){
    a = 31 ;
    b = 11 ;
    c = 30 ;
    p = 0 ; 
    for (int i = 0; i<16; i++){
        s[i] = uniform.randInt();
    }
}

prngXorshift1024::~prngXorshift1024(){}

randtype prngXorshift1024::xorshift1024star(){
    randtype s0 = s[p] ;
    randtype s1 = s[p = (p+1) & 15] ;
    s1 ^= s1 << a ;
    s[p] = s1 ^ s0 ^ ( s1 >> b) ^ ( s0 >> c ) ;
    return s[p] * 0x9e3779b97f4a7c13 ;
}

double prngXorshift1024::rand(double min, double max){
    return (double) (max - min) * ( (double) xorshift1024star() ) 
        / ( (double) ULLONG_MAX ) + min ;
}