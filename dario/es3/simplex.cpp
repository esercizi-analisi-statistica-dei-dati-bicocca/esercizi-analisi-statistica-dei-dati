#include "point.h"
#include "simplex.h"
#include "prngXoroshiro128.h"
#include <vector>
#include <gsl/gsl_math.h> // M_PI
#include <cmath> // pow, cos
#include <iostream>

#define BUKIN6

struct minimumrange {
    double xmin ;
    double xmax ;
    double ymin ;
    double ymax ;
} ;

#ifdef EASOM
double f (Point<double, 2> p){
    // easom
    // - cos(x) * cos(y) * exp( -( (x-pi)^2 + (y-pi)^2 ) )
    return - cos(p[0]) * cos(p[1]) * 
        exp( -( pow( p[0]-M_PI , 2 ) + pow( p[1]-M_PI , 2 ) ) );
}
minimumrange frange {
    .xmin = 2.,
    .xmax = 7 ,
    .ymin = 2.,
    .ymax = 7.
} ;
#endif

#ifdef GOLDSTEINPRICE
double f (Point<double, 2> p){
    // goldstein-price
    // exp(0.5*(x^2+y^2-25)^2)+(sin(4x-3y))^4+0.5*(2x+y-10)^2
    double add1 = p[0]*p[0] + p[1]*p[1] - 25 ;
    double add2 = sin( 4*p[0] - 3*p[1] ) ;
    double add3 = 2*p[0] + p[1] - 10 ;
    return exp( 0.5*add1*add1 ) + add2*add2*add2*add2 + 0.5*add3*add3 ;
}
minimumrange frange {
    .xmin = -5.,
    .xmax = 6.5 ,
    .ymin = -5.,
    .ymax = 6.5
} ;
#endif

#ifdef BUKIN6
double f (Point<double, 2> p){
    // bukin no. 6
    // 100*sqrt(abs(y-0.01*x^2))+0.01*abs(x+10)
    return 100 * sqrt( fabs( p[1] - 0.01*p[0]*p[0] ) ) + 0.01*fabs(p[0]+10) ;
}
minimumrange frange {
    .xmin = -15.,
    .xmax = -5 ,
    .ymin = -3.,
    .ymax = 3.
} ;
#endif

#ifdef BOOTH
double f (Point<double, 2> p){
    // booth
    // (x+2y-7)^2+(2x+y-5)^2
    double add1 =   p[0] + 2*p[1] - 7 ;
    double add2 = 2*p[0] +   p[1] - 5 ;
    return add1*add1 + add2*add2 ;
}
minimumrange frange {
    .xmin = -10.,
    .xmax = 10 ,
    .ymin = -10.,
    .ymax = 10.
} ;
#endif

int main(int numArg, char* listArg[]) {

    int points    = 1000 ;
    double delta1 = 0.1 ;
    double delta2 = 0.05 ;
    int maxsteps  = 3000 ;
    double ftol   = 1e-30;
    double ptol   = 1e-15;
    if (numArg >=2) { points   = std::stoi( listArg[1] ); }
    if (numArg >=3) { delta1   = std::stod( listArg[2] ); }
    if (numArg >=4) { delta2   = std::stod( listArg[3] ); }
    if (numArg >=5) { maxsteps = std::stoi( listArg[4] ); }
    if (numArg >=6) { ftol     = std::stod( listArg[5] ); }
    if (numArg >=7) { ptol     = std::stod( listArg[6] ); }

    std::cout << "xstart,ystart,xtemp,ytemp,xmin,ymin,f@min,iters,ftol,ptol" 
        << std::endl ;

    prngXoroshiro128 generator;
    for (int i=0; i<points; i++) {
        double xin = generator.rand(frange.xmin, frange.xmax) ;
        double yin = generator.rand(frange.ymin, frange.ymax) ;
        std::cout << xin << "," << yin << "," ;
        double input[] = {xin , yin} ;
        Point<double, 2> p0(input) ;

        try {
            Simplex<double, 2> simplesso_temp (f);
            Point<double, 2> temp;
            temp = simplesso_temp.minimize(p0, delta1, ftol, ptol, maxsteps);
             Simplex<double, 2> simplesso (f);
            Point<double, 2> minimum;
            minimum = simplesso.minimize(temp, delta2, ftol, ptol, maxsteps);
            std::cout << temp.component(0)<<","<<temp.component(1)<< "," ;
            std::cout << minimum.component(0)<<","<<minimum.component(1)<<",";
            std::cout << f(minimum) << "," ;
            simplesso.printResult();
        } catch (...) {
            std::cout << ",,,,,,," << std::endl ;
        }

    }

    return 0;
}