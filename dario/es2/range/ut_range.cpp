/*
g++ ut_range ut_range.cpp
*/

#include "range.h"

int main (int numArg, char* listArg[]){

    int start = 40000000 ;
    int end   = 50000000 ;
    int step  = 1000000 ;
    for (auto i: range(start, end, step)) {
        std::cout << i << std::endl ;
    }
    std::cout << std::endl ;

    int startint = 10 ;
    int endint   = 100 ;
    int densint  = 10 ;
    for (auto i: logrange(startint, endint, densint)) {
        std::cout << i << std::endl ;
    }
    std::cout << std::endl ;


    long int startlong = 100 ;
    long int endlong   = 1000 ;
    long int denslong  = 10 ;
    for (auto i: logrange(startlong, endlong, denslong)) {
        std::cout << i << std::endl ;
    }

    return 0;
}