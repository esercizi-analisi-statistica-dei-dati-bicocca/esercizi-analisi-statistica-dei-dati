#include "mc.h"
#include "prngXoroshiro128.h"
#include <iostream>
#include <cmath>

int main () {

    mc<prngXoroshiro128> montecarlo(10000);
    std::cout << montecarlo.f_mc_short() << std::endl;
    montecarlo.f_mc();
    std::cout << montecarlo.f_get_integral() << ", "
        << montecarlo.f_get_stdev()
        << std::endl;
    montecarlo.set_parameters(100000);
    montecarlo.f_mc();
    std::cout << montecarlo.f_get_integral() << ", "
        << montecarlo.f_get_stdev()
        << std::endl;

    return 0;
}