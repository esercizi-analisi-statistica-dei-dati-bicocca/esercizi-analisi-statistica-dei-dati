/*
g++ -o signalbackground signalbackground.cpp -g `root-config --glibs --cflags`
*/

#include <iostream>
#include <cmath>

#include "TCanvas.h"
#include "TH2F.h"
#include "TFile.h"
#include "TTree.h"
#include "TStyle.h"

double gauss (double x, double y, double mux, double muy, double sigmax, double sigmay, double rho){
    double auxrho = 1 - rho*rho ;
    double auxmux = x - mux ;
    double auxmuy = y - muy ;
    double auxmux2 = auxmux / sigmax ;
    double auxmuy2 = auxmuy / sigmay ;
    double norm = 1. / ( 2 * M_PI * sigmax*sigmay * sqrt( auxrho ) ) ;
    double exponent = -1. / ( 2*auxrho ) * ( auxmux2*auxmux2 + auxmuy2*auxmuy2 - 2*rho*auxmux2*auxmuy2  ) ;
    return norm * exp( exponent ) ;
}

double cauchy (double x, double peak , double hwhm){
    double gamma = sqrt( peak*peak * ( peak*peak + hwhm*hwhm ) ) ;
    double k = ( 2*sqrt(2) * peak * hwhm * gamma) 
        / (M_PI * sqrt( peak*peak + gamma )) ;
    return k / ( ( x*x - peak*peak ) * ( x*x - peak*peak ) 
        + peak*peak * hwhm*hwhm ) ;
}

double gauscauchy (double x, double y, 
    double mux, double sigmax, double peak, double hwhm, double theta) {
    double newx = x*cos(theta) - y*sin(theta);
    double newy = x*sin(theta) + y*cos(theta);
    x = newx;
    y = newy;
    double exponent = -1. * (x*x - mux*mux)*(x*x - mux*mux) / (2*sigmax*sigmax) ;
    return 1. / (sqrt( 2 * M_PI * sigmax )) * exp (exponent) * cauchy(y, peak, hwhm) ;
}

// double gaussroot (double* x, double* par) {
//     return gauss (x[0], x[1], par[0], par[1], par[2], par[3], par[4] ) ;
// }

double randunif (double xmin=0, double xmax=1) {
    return (xmax - xmin) * ( (double) rand() / RAND_MAX) + xmin ;
}

struct point {
    double x;
    double y;
};

point gausrand (
        double mux, double muy, double sigmax, double sigmay, double rho, 
        double xmin, double xmax, double ymin, double ymax) {
    point randomGauss;
    while(1){
        randomGauss.x = randunif (xmin, xmax);
        randomGauss.y = randunif (ymin, ymax);
        double z = randunif ();
        double fxy = gauss(randomGauss.x, randomGauss.y, mux, muy, sigmax, sigmay, rho);
        // std::cout << " " << fxy << "," << z  << std::endl ;
        if ( fxy > z ) {
            return randomGauss;
        }
    }
}

point gauscauchyrand (
        double mux, double sigmax, double peak, double hwhm, double theta,
        double xmin, double xmax, double ymin, double ymax) {
    point randomPoint;
    while(1){
        randomPoint.x = randunif (xmin, xmax);
        randomPoint.y = randunif (ymin, ymax);
        double z = randunif ();
        double fxy = gauscauchy(randomPoint.x, randomPoint.y, mux, sigmax, peak, hwhm, theta);
        if ( fxy > z ) {
            return randomPoint;
        }
    }
}


int main () {

    TCanvas* c1 = new TCanvas ("c1", "c1", 700, 500);

    // signal
    double mux1    = 0. ;
    double muy1    = 1. ;
    double sigmax1 = 1.5 ;
    double sigmay1 = 1.3 ;
    double rho1    = 0.7 ;
    // double mux1    = -1. ;
    // double muy1    = 1. ;
    // double sigmax1 = 0.8 ;
    // double sigmay1 = 0.7 ;
    // double rho1    = 0.7 ;

    // bg
    double mux    = 0. ;
    double sigmax = 1.0 ;
    double peak   = 1.5 ;
    double hwhm   = 0.3 ;
    double theta  = -0.7 ;

    // boundaries
    double xmin = -10;
    double xmax = 10;
    double ymin = -10;
    double ymax = 10;

    // signal dataset size
    int signalEvents = 10000;
    int backgroundEvents = 10000;

    double sigX;
    double sigY;
    double bgX;
    double bgY;

    TFile* outfile = new TFile("sigbg0.root", "RECREATE");
    TTree* sigTree = new TTree("Signal", "Gaussian Signal");
    sigTree->Branch("x", &sigX);
    sigTree->Branch("y", &sigY);
    TTree* bgTree = new TTree("Background", "Gaussian Background");
    bgTree->Branch("x", &bgX);
    bgTree->Branch("y", &bgY);

    TH2F* gauss1 = new TH2F ("gauss1", "signal (red) - background (blue)", 
        100, xmin, xmax, 100, ymin, ymax);
    TH2F* gauss2 = new TH2F ("gauss2", "background", 
        100, xmin, xmax, 100, ymin, ymax);
    point gaussian2drandom;
    for (int i=0; i<signalEvents; i++){
        gaussian2drandom = gausrand(mux1,muy1,sigmax1,sigmay1,rho1,
            xmin,xmax,ymin,ymax);
        gauss1->Fill(gaussian2drandom.x, gaussian2drandom.y);
        sigX = gaussian2drandom.x ;
        sigY = gaussian2drandom.y ;
        sigTree->Fill();
    }
    for (int i=0; i<backgroundEvents; i++){
        gaussian2drandom = gauscauchyrand(mux,sigmax,peak,hwhm,theta,
            xmin,xmax,ymin,ymax);
        gauss2->Fill(gaussian2drandom.x, gaussian2drandom.y);
        bgX = gaussian2drandom.x ;
        bgY = gaussian2drandom.y ;
        bgTree->Fill();
    }
    gStyle->SetOptStat(0);
    gauss1->SetMarkerStyle(7);
    gauss1->SetMarkerSize(3);
    gauss1->SetMarkerColor(kRed);
    gauss1->Draw();
    gauss2->SetMarkerStyle(7);
    gauss2->SetMarkerSize(3);
    gauss2->SetMarkerColor(kBlue);
    gauss2->Draw("SAME");
    c1->Print("sigbg0.eps", "eps");
    c1->Print("sigbg0.png", "png");

    // lasciarlo dopo il print, altrimenti c'è un segfault
    sigTree->Write();
    bgTree->Write();
    outfile->Close();

    return 0;
}