#include "Experiment.h"

#include <iostream>
#include <sstream>
#include <string>
#include <cmath>

#include "RooUnfold.h"
#include "RooUnfoldResponse.h"
#include "RooUnfoldBayes.h"
#include "TH1D.h"
#include "TCanvas.h"
#include "TLegend.h"

int main(int argc , char* argv[]){
    // ----- PARAMETERS -----
    // physical quantities (I.S.)
    double lambda = 400e-9 ;
    int slits = 5. ;
    double slitsize = 0.75e-6 ;
    double slitdistance = 4e-4 ;
    double screenDistance = 0.50 ;

    // toy experiment parameters
    int experimentSize = 50000;
    double smearingFactor = 5 ;

    // analysis variables, related to the previous parameters!
    int bins = 100;
    double xmin = -3;
    double xmax =  3;

    // ----- ALGORITHM -----
    Experiment lightslits
        (experimentSize, smearingFactor,
        lambda, slits, slitsize, slitdistance, screenDistance );

    // ideal experiment
    double * idealData = lightslits.sample_hit_or_miss(xmin, xmax );
    // theoretical plausible experiment
    double * smearedData = lightslits.smearing( xmin, xmin, bins ) ;
    // measured data. this is a toy experiment, the "true" data
    // is generated in the same way of the plausible experiment 
    double * idealDataDummy = lightslits.sample_hit_or_miss(xmin, xmax );
    double * realData = lightslits.smearing( xmin, xmax, bins );

    TCanvas* c1 = new TCanvas("unfolding", "unfolding", 1100, 800);
    // histogram with ideal data
    TH1D* intensityIdeal = new TH1D("intensityIdeal", "", bins, xmin, xmax);
    // histogram with smeared data
    TH1D* intensityMeasured = new TH1D("measured", "measured", 
        bins, xmin, xmax);
   
    // Response matrix of RooUnfold
    // combines the ideal data and the smeared data
    RooUnfoldResponse response(bins, xmin, xmax);
    for (int i = 0; i < experimentSize ; i++ ){
        intensityIdeal->Fill(idealData[i]);
        intensityMeasured->Fill(realData[i]);
        response.Fill(smearedData[i], idealData[i]);
    }
    // Unfolding by Bayesian iteration of RooUnfold
    // uses the response matrix to unflod the measured data
    RooUnfoldBayes  unfold (&response, intensityMeasured, 4);
    TH1D* hist_reco = (TH1D*) unfold.Hreco();

    // ----- DRAWING -----
    intensityIdeal->GetXaxis()->SetTitle("X (m)");
    intensityIdeal->GetYaxis()->SetTitle("Intensity");
    intensityIdeal->SetLineWidth(2);
    intensityIdeal->SetLineColor(kBlue);
    TLegend * legend = new TLegend();
    legend->AddEntry(intensityIdeal, "f_true", "l");
    intensityIdeal->Draw();

    legend->AddEntry(intensityMeasured, "f_smeared", "l");
    intensityMeasured->SetLineWidth(2);
    intensityMeasured->SetLineColor(kGreen);
    intensityMeasured->Draw("SAME");

    hist_reco->SetLineColor(kRed);
    hist_reco->SetLineWidth(2);
    hist_reco->SetMarkerSize(7);
    legend->AddEntry(hist_reco, "f_unfolded", "lp");
    hist_reco->Draw("SAME");
    legend->Draw("SAME");
    c1->Draw();

    std::ostringstream stringStream;
    stringStream << smearingFactor << "-" 
        << slitsize << "-" << slitdistance << "-" << screenDistance ;
    std::string copyOfStr = stringStream.str();
    c1->Print(("data/unfold-"+ copyOfStr +".pdf").c_str(), "pdf");
}