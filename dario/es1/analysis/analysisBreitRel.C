double BreitWignerRel(double x, double peak , double hwhm){
    double pi = atan(1)*4;
    double gamma = sqrt( peak*peak * ( peak*peak + hwhm*hwhm ) ) ;
    double k = ( 2*sqrt(2) * peak * hwhm * gamma) 
        / (pi * sqrt( peak*peak + gamma )) ;
    return k / ( ( x*x - peak*peak ) * ( x*x - peak*peak ) 
        + peak*peak * hwhm*hwhm ) ;
}

void analysisBreitRel ( string filename = "../data/breitreltac.txt" ) {
    std::fstream breitRelFile;
    std::cout << filename << std::endl;
    breitRelFile.open(filename);

    double min = 0;
    double max = 20;
    int bins = 250;

    TCanvas* c1 = new TCanvas("breitRel", 
        "Relativistic Breit-Wigner", 700, 700);
    TH1D *breitRelHisto = new TH1D ("breitRel", 
        "Breit-Wigner Relativistic Histogram", bins, min, max);
    double breitRelX;
    while (breitRelFile >> breitRelX){
        breitRelHisto -> Fill(breitRelX);
    }
    // the effective entries used to compute the normalization of the pdf are
    // the entries of the histogram, without the overflow and the underflow
    int entries = breitRelHisto->GetEntries() 
        - breitRelHisto->GetBin(bins+1) - breitRelHisto->GetBin(0);

    double normalization = entries * (max - min) / bins;
    std::cout << "norm: " << normalization << std::endl;
    TF1 *breitRelPdf = new TF1 ("breitRelFunc", 
        "[2]*BreitWignerRel(x,[0], [1])", min, max);
    breitRelPdf->SetParameters(10000, 0., 1.);
    breitRelPdf->SetParName(0, "#\Gamma");
    breitRelPdf->SetParameter(0,2.);
    breitRelPdf->SetParName(1, "M");
    breitRelPdf->SetParameter(1,5.);
    breitRelPdf->SetParName(2, "norm");
    breitRelPdf->SetParameter(2,normalization);
    breitRelHisto->Fit("breitRelFunc");
    //                 ksiourmen
    gStyle->SetOptStat(  1110011);
    gStyle->SetOptFit(1111);
    breitRelHisto->Draw();

    cout << filename << endl;
    filename.erase( std::remove(filename.begin(), 
        filename.end()-4, '.'), filename.end() );
    std::replace(filename.begin()+1, filename.end(), '/', '-');
    filename.erase( std::remove(filename.begin(), 
        filename.end(), '/'), filename.end() );
    cout << filename << endl;

    c1->Print( filename.c_str(), "pdf");

}