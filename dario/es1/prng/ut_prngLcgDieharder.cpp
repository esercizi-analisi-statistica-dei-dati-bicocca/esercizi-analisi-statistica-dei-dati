#include <iostream>
#include <string>
#include <bitset>
#include <cstdio>
#include "prngLcg.h"

int main(int numArg, char* listArg[]) {

    prngLcg generator;
    randtype randomnumber = generator.randInt();
    std::cout.write(reinterpret_cast<char*>(&randomnumber),
         sizeof randomnumber) ;

    while(1) {
        randomnumber = generator.randInt(); 
        std::cout.write(reinterpret_cast<char*>(&randomnumber),
             sizeof randomnumber) ;
    }

    return 0;
}
