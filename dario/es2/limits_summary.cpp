/*
Check the current architecture to see what can be achieved on the current machine.

http://en.cppreference.com/w/cpp/types/climits
*/

#include <iostream>
//#include <limits>
#include<cfloat>

int main () {

    std::cout << "radix: " << FLT_RADIX << std::endl << std::endl ;

    std::cout << "float: "                  << std::endl
        << " min: "         << FLT_MIN        << std::endl
        << " max: "         << FLT_MAX        << std::endl
        << " epsilon: "     << FLT_EPSILON    << std::endl
        << " dig: "         << FLT_DIG        << std::endl
        << " mant dig: "    << FLT_MANT_DIG   << std::endl
        << " exp min bin: " << FLT_MIN_EXP    << std::endl
        << " exp max bin: " << FLT_MAX_EXP    << std::endl
        << " exp min dec: " << FLT_MIN_10_EXP << std::endl
        << " exp max dec: " << FLT_MAX_10_EXP << std::endl
        << std::endl ;

    std::cout << "double: "                  << std::endl
        << " min norm:    " << DBL_MIN        << std::endl
        << " max norm:    " << DBL_MAX        << std::endl
        << " epsilon:     " << DBL_EPSILON    << std::endl
        << " dig:         " << DBL_DIG        << std::endl
        << " mant dig:    " << DBL_MANT_DIG   << std::endl
        << " exp min bin: " << DBL_MIN_EXP    << std::endl
        << " exp max bin: " << DBL_MAX_EXP    << std::endl
        << " exp min dec: " << DBL_MIN_10_EXP << std::endl
        << " exp max dec: " << DBL_MAX_10_EXP << std::endl
        << std::endl ;

    std::cout << "long double: "                  << std::endl
        << " min norm:    " << LDBL_MIN        << std::endl
        << " max norm:    " << LDBL_MAX        << std::endl
        << " epsilon:     " << LDBL_EPSILON    << std::endl
        << " dig:         " << LDBL_DIG        << std::endl
        << " mant dig:    " << LDBL_MANT_DIG   << std::endl
        << " exp min bin: " << LDBL_MIN_EXP    << std::endl
        << " exp max bin: " << LDBL_MAX_EXP    << std::endl
        << " exp min dec: " << LDBL_MIN_10_EXP << std::endl
        << " exp max dec: " << LDBL_MAX_10_EXP << std::endl
        << std::endl ;

    return 0;
}