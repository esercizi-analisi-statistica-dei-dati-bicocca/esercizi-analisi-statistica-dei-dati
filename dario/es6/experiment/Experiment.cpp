#include "Experiment.h"
#include <algorithm>

Experiment::Experiment(
        int experimentSize, double smearingFactor ,
        double newlambda, int slits,
        double slitsize, double slitdistance, double screenDistance)
{
    Nsamp = experimentSize;
    lambda = newlambda;
    Nfend = slits ;
    A = slitsize ;
    D = slitdistance ;
    L = screenDistance ;
    SMfactor = smearingFactor ;
}

Experiment::~Experiment(){
    delete idealData;
    delete smearedData;
}

double Experiment::spettro(double x){
    double theta = atan(x / L);
    double beta = (M_PI * A / lambda) * sin(theta);
    double gamma = (M_PI * D / lambda) * sin(theta);
    return pow((sin(beta)/ beta) * (sin(Nfend*gamma)/ sin(gamma)) ,2);
}

double* Experiment::sample_hit_or_miss(double xmin, double xmax){
    idealData = new double[Nsamp];
    int i = 0;
    while(i < Nsamp){
        double x = rnd.rand(xmin, xmax);
        double y = rnd.rand();
        if ( y < spettro(x)){
            idealData[i] = x;
            i++;
        }
    }
    return idealData;
}

double* Experiment::smearing( double xmin, double xmax, int bins ){
    double sigma = SMfactor * (xmax - xmin) / bins;
    smearedData = new double[Nsamp];
    for(int i = 0; i < Nsamp; i++){
        double x = idealData[i];
        smearedData[i] = x + rnd.gauss(x, sigma);
    }
    return smearedData;
}

