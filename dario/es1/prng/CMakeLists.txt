cmake_minimum_required(VERSION 2.6)
project(prng)

add_library(prngLcg prngLcg.cpp)

add_library(prngGen prngGen.cpp)
target_link_libraries(prngGen prngLcg)

foreach( PRNGLIB prngXorshift128 prngXorshift1024 prngXoroshiro128)
    add_library(${PRNGLIB} ${PRNGLIB}.cpp)
    target_link_libraries(${PRNGLIB} prngLcg)
    foreach(UTEXE ut_${PRNGLIB} ut_${PRNGLIB}Dieharder)
        add_executable(${UTEXE} ${UTEXE}.cpp)
        target_link_libraries(${UTEXE} ${PRNGLIB})
    endforeach(UTEXE)
endforeach(PRNGLIB)

foreach(UTEXE ut_prngLcg ut_prngLcgDieharder)
    add_executable(${UTEXE} ${UTEXE}.cpp)
    target_link_libraries(${UTEXE} prngLcg)
endforeach(UTEXE)

foreach(UTEXE ut_prngGenInv ut_prngGenTac ut_prngGenRelTac)
    add_executable(${UTEXE} ${UTEXE}.cpp)
    target_link_libraries(${UTEXE} prngGen)
endforeach(UTEXE)
