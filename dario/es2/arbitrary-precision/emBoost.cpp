/*
The parameter of cpp_bon_float<int> is the desired
number of exact decimal numbers that type should
guarantee (aka digits10).

if the associated number of digits is less than 128, then
this program is surprisingly slower

this requires only the boost libraries (libboost-all-dev)

https://www.boost.org/doc/libs/1_66_0/libs/multiprecision/doc/html/index.html
*/

#include <boost/multiprecision/cpp_bin_float.hpp>
#include <iostream>
#include <iomanip>
#include <string>
#include <boost/math/constants/constants.hpp>

typedef boost::multiprecision::
    number<boost::multiprecision::cpp_bin_float<77> > myfloat;

int main()
{
   std::cout << std::numeric_limits<myfloat>::digits << std::endl;
   std::cout << std::numeric_limits<myfloat>::digits10 << std::endl;

   myfloat sum = 0;
   myfloat result = 0;
   myfloat residual = 0;

    long int n = 50000000 ;
    for (long int i=1; i<n; i++){
        sum += 1./(double)i ;
    }
    result = sum - log( (myfloat) n);
    residual = boost::math::constants::euler<myfloat>() - result;

    std::cout 
        << std::setprecision(std::numeric_limits<myfloat>::max_digits10)
        << result << "," << residual 
        << std::endl;
    return 0;
}